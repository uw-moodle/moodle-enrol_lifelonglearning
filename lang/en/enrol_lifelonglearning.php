<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_lifelonglearning', language 'en'.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['coursecreation'] = 'Course creation';
$string['defaultcategorydesc'] = 'Lifelong learning category';
$string['dontsync'] = 'Do not sync';
$string['duplicatespaceid'] = 'Error:  This space ID is already linked to course "{$a->course}".';
$string['enrolnotenabled'] = 'Lifelong learning enrollments not enabled.';
$string['errornotconfigured'] = 'Lifelong learning plugin not configured';
$string['enrolstatus'] = 'Lifelong learning enrollment status.';
$string['enrolstatus_help'] = 'This page shows the LMS space parameters passed from CSIS.';
$string['ferpastudentrole_key'] = 'Ferpa-protected student role';
$string['instancename'] = 'UW lifelong learning';
$string['unknownlmstarget'] = 'Unknown lmsTarget';
$string['lifelonglearning:triggerevent'] = 'Trigger an event via webservice';
$string['lifelonglearning:config'] = 'Configure lifelong learning instances';
$string['lifelonglearning:unenrol'] = 'Unenroll inactive course users';
$string['lmstarget_key'] = 'LMS Target Id';
$string['lmstarget'] = 'Comma-separated list of lifelong learning LMS target ids.';
$string['nolmstargets'] = 'Error: No LMS targets defined in site config.';
$string['nospaces'] = 'No unprovisioned spaces found.';
$string['pluginname'] = 'UW lifelong learning';
$string['pluginname_desc'] = 'The UW lifelong learning plugin handles L3 enrollments from CAOS.';
$string['pluginnotenabled'] = 'Lifelong learning enrollments not enabled';
$string['redirectnospace'] = 'The course does not exist on this server.';
$string['rolemapping'] = 'Role mapping';
$string['schemavalidationexception'] = 'Schema validation exception: {$a}';
$string['serversettings'] = 'Data source settings';
$string['studentrole_key'] = 'Student role';
$string['soappass_key'] = 'Password';
$string['soapurl_key'] = 'SOAP URL';
$string['soapuser_key'] = 'Username';
$string['spaceid'] = 'Space id';
$string['spaceselect'] = 'Unprovisioned CSIS spaces';
$string['syncerror'] = 'An error occured while syncing space "{$a}".  The enrollment instance has been created, but please ensure that the space id is correct.';
$string['syncnow'] = 'Synchronize space with CSIS';
$string['teacherrole_key'] = 'Teacher role';
$string['template'] = 'Optional: auto-created courses can copy their settings from a template course.  Specify the course shortname.';
$string['template_key'] = 'Template';
$string['unsyncedspacetitle'] = 'Unknown - Space not yet synced';