<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add instance form fpr lifelonglearning enrolment.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

class enrol_lifelonglearning_addinstance_form extends moodleform {
    protected $course;

    function definition() {
        $mform  = $this->_form;
        $course = $this->_customdata['course'];
        $spaces = $this->_customdata['spaces'];
        $this->course = $course;

        $mform->addElement('header','general', get_string('pluginname', 'enrol_lifelonglearning'));

        /* @var $enrol enrol_lifelonglearning_plugin */
        $enrol = enrol_get_plugin('lifelonglearning');

        $lmstargets = $enrol->get_lmstargets();
        if (empty($lmstargets)) {
            print_error('nolmstargets', 'enrol_lifelonglearning');
        }

        $mform->addElement('select', 'space', get_string('spaceselect', 'enrol_lifelonglearning'), $spaces);
        $mform->addRule('space', get_string('required'), 'required', null, 'client');

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons(true, get_string('addinstance', 'enrol'));

        $this->set_data(array('id'=>$course->id));
    }
}

