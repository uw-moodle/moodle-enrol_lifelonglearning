<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Status of an instance of lifelonglearning enrolment.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_lifelonglearning\local\caos\caos_datasource;
use local_wiscservices\local\uds\uds_datasource;
use enrol_lifelonglearning\local\caos\schema\space;
use enrol_lifelonglearning\local\caos\schema\space_status_enum;
use enrol_lifelonglearning\lmsspace;

define('NO_OUTPUT_BUFFERING', true);

require('../../config.php');

$id = required_param('id', PARAM_INT); // instance id
$syncnow = optional_param('syncnow', 0, PARAM_INT);

$instance = $DB->get_record('enrol', array('id'=>$id, 'enrol'=>'lifelonglearning'), '*', MUST_EXIST);
$course = $DB->get_record('course', array('id'=>$instance->courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

$thisurl = new moodle_url('/enrol/lifelonglearning/status.php', array('id'=>$instance->id));
$PAGE->set_url($thisurl);
$PAGE->set_pagelayout('admin');

$returnurl = new moodle_url('/enrol/instances.php', array('id'=>$course->id));

//navigation_node::override_active_url(new moodle_url('/enrol/instances.php', array('id'=>$course->id)));

require_login($course);
require_capability('moodle/course:enrolreview', $context);

/* @var $enrol enrol_lifelonglearning_plugin */
$enrol = enrol_get_plugin('lifelonglearning');

$lmsspace = lmsspace::load_space_by_instance($instance);

if ($syncnow) {
    require_sesskey();
    $caos = new caos_datasource();
    $uds = new uds_datasource();
    $trace = new html_progress_trace();

    echo $OUTPUT->header();
    $trace->output('Starting sync...');
    try {
        $trace->output('Querying space.');
        $spaces = $caos->getSpaces($lmsspace->get_lmstarget());
        $ourspace = null;
        foreach ($spaces as $space) {
            if ($space->id == $lmsspace->get_spaceid()) {
                $ourspace = $space;
                break;
            }
        }
        if ($ourspace) {
            $trace->output('Updating space meta data.');
            $lmsspace->update($space);
        } else {
            $trace->output('Space not found in data source.  Skipping Update.');
        }
        $trace->output('Querying enrollments.');
        $enrol->sync_space_enrollment($trace, $lmsspace, $caos, $uds, true);
    } catch (\Exception $e) {
        $trace->output($e->getMessage());
    }
    $trace->output('Done');
    echo $OUTPUT->continue_button($thisurl);
    echo $OUTPUT->footer();
    die;
}

$PAGE->set_heading($course->fullname);
$PAGE->set_title($enrol->get_instance_name($instance));

echo $OUTPUT->header();
echo $OUTPUT->heading_with_help($enrol->get_instance_name($instance), 'enrolstatus', 'enrol_lifelonglearning');

$table = new html_table();

$rows = array();
$rows[] = array('Space id', $lmsspace->get_spaceid());
$rows[] = array('LMS target', $lmsspace->get_lmstarget());
$rows[] = array('Initial title', $lmsspace->get_title());
$rows[] = array('Categories', $lmsspace->get_categories());

$active = $DB->count_records('user_enrolments', array('enrolid' => $id, 'status' => ENROL_USER_ACTIVE));
$suspended = $DB->count_records('user_enrolments', array('enrolid' => $id, 'status' => ENROL_USER_SUSPENDED));
$rows[] = array('Active enrollments', $active);
$rows[] = array('Suspended enrollments', $suspended);

$time = $lmsspace->get_time_fullsync();
$rows[] = array('Last full sync', $time? userdate($time) : get_string('never'));
$time = $lmsspace->get_time_rosterupdate();
$rows[] = array('Last roster update', $time? userdate($time) : get_string('never'));

$table->data = $rows;
echo html_writer::table($table);
$syncurl = new moodle_url($thisurl, array('syncnow' => 1));
echo $OUTPUT->single_button($syncurl, get_string('syncnow', 'enrol_lifelonglearning'), 'post');
echo $OUTPUT->single_button($returnurl, get_string('back'), 'get');

echo $OUTPUT->footer();
