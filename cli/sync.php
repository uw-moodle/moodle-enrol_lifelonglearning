<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * CLI client for a full Lifelong learning enrolment plugin sync.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 *
 * This script is meant to be called from a cronjob to sync moodle with the L3 roter data.
 *
 * Example cron entry:
 * # 5 minutes past 4am
 * 5 4 * * * php /var/www/moodle/enrol/lifelonglearning/cli/sync.php
 *
 * Notes:
 *   - If you have a large number of users, you may want to raise the memory limits
 *     by passing -d momory_limit=256M
 *   - For debugging & better logging, you are encouraged to use in the command line:
 *     -d log_errors=1 -d error_reporting=E_ALL -d display_errors=0 -d html_errors=0
 *
 */

use enrol_lifelonglearning\local\caos\caos_datasource;
use local_wiscservices\local\uds\uds_datasource;

define('CLI_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');

// Ensure errors are well explained
$CFG->debug = DEBUG_NORMAL;

if (!enrol_is_enabled('lifelonglearning')) {
    error_log(get_string('pluginnotenabled', 'enrol_lifelonglearning'));
    die;
}

// Update enrolments

/* @var $enrol enrol_lifelonglearning_plugin */
$enrol = enrol_get_plugin('lifelonglearning');

$caos = new caos_datasource();
$uds = new uds_datasource();

$trace = new text_progress_trace();

foreach ($enrol->get_lmstargets() as $lmstarget) {
    $enrol->sync_all_spaces($trace, $lmstarget, $caos, $uds);
}

$trace->output("Done");
$trace->finished();
