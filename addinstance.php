<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add instance of lifelonglearning enrolment.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_lifelonglearning\local\caos\caos_datasource;
use local_wiscservices\local\uds\uds_datasource;
use enrol_lifelonglearning\local\caos\schema\space;
use enrol_lifelonglearning\local\caos\schema\space_status_enum;
use enrol_lifelonglearning\lmsspace;

require('../../config.php');
require_once("$CFG->dirroot/enrol/lifelonglearning/addinstance_form.php");

$id = required_param('id', PARAM_INT); // course id

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

$PAGE->set_url('/enrol/lifelonglearning/addinstance.php', array('id'=>$course->id));
$PAGE->set_pagelayout('admin');

navigation_node::override_active_url(new moodle_url('/enrol/instances.php', array('id'=>$course->id)));

require_login($course);
require_capability('moodle/course:enrolconfig', $context);

/* @var $enrol enrol_lifelonglearning_plugin */
$enrol = enrol_get_plugin('lifelonglearning');
if (!$enrol->get_newinstance_link($course->id)) {
    redirect(new moodle_url('/enrol/instances.php', array('id'=>$course->id)));
}

// Find spaces in use.
$inuse = array();
$otherenrols = $DB->get_records('enrol', array('enrol' => 'lifelonglearning'));
foreach ($otherenrols as $otherenrol) {
    $inuse[$otherenrol->{lmsspace::LMSTARGET_FIELD}][$otherenrol->{lmsspace::SPACEID_FIELD}] = $otherenrol->courseid;
}

// Query all spaces.
$caos = new caos_datasource();

$lmstargets = $enrol->get_lmstargets();
if (empty($lmstargets)) {
    print_error('nolmstargets', 'enrol_lifelonglearning');
}

$choices = array();
foreach ($lmstargets as $lmstarget) {
    $spaces = $caos->getSpaces($lmstarget);
    foreach ($spaces as $space) {
        if (!empty($inuse[$space->lmsTarget][(string)$space->id])) {
            continue;
        }
        $key = $space->id . ',' . $space->lmsTarget;
        $name = '';
        if (count($lmstargets) > 1) {
            $name = $space->lmsTarget.' / ';
        }
        $name .= $space->id.': ';
        $name .= $space->spaceProvisioning->title;
        $choices[$key] = s($name);
    }
}

if (empty($choices)) {
    echo $OUTPUT->header();
    echo $OUTPUT->notification(get_string('nospaces', 'enrol_lifelonglearning'));
    echo $OUTPUT->continue_button(new moodle_url('/enrol/instances.php', array('id'=>$course->id)));
    echo $OUTPUT->footer();
}

$mform = new enrol_lifelonglearning_addinstance_form(NULL, array('course'=>$course, 'spaces'=>$choices));

if ($mform->is_cancelled()) {
    redirect(new moodle_url('/enrol/instances.php', array('id'=>$course->id)));

} else if ($data = $mform->get_data()) {
    list($spaceid, $lmstarget) = explode(',', $data->space, 2);
    $space = new space(array(
            'id' => $spaceid,
            'lmsTarget' => $lmstarget,
            'spaceProvisioning' => array(
                    'title' => get_string('unsyncedspacetitle', 'enrol_lifelonglearning'),
            )
    ));
    $existing = lmsspace::load_space($space);
    if ($existing) {
        // Duplicate enrollment, print an error.
        echo $OUTPUT->header();
        $existingcourse = $DB->get_record('course', array('id'=>$existing->get_enrol_instance()->courseid));
        $instancelink = html_writer::link(new moodle_url('/enrol/lifelonglearning/status.php', array('id'=>$existing->get_enrol_instance()->id)), $existingcourse->fullname);
        echo $OUTPUT->box(get_string('duplicatespaceid', 'enrol_lifelonglearning', array('course'=>$instancelink)));
        echo $OUTPUT->single_button(new moodle_url('/enrol/instances.php', array('id'=>$course->id)), get_string('continue'));
        echo $OUTPUT->footer();
        die;
    }
    // Create the enrollment.
    $lmsspace = lmsspace::create_space($space, $course);

    // Redirect to enrollment sync page.
    redirect(new moodle_url('/enrol/lifelonglearning/status.php',
                array('id'=>$lmsspace->get_enrol_instance()->id, 'syncnow'=>1, 'sesskey'=>sesskey())));
}

$PAGE->set_heading($course->fullname);
$PAGE->set_title(get_string('pluginname', 'enrol_lifelonglearning'));

echo $OUTPUT->header();

$mform->display();

echo $OUTPUT->footer();
