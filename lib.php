<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * University of Wisconsin, Madison lifelong learning enrolment plugin.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_lifelonglearning\local\caos\datasource as caos_datasource_interface;
use local_wiscservices\local\uds\datasource as uds_datasource_interface;
use enrol_lifelonglearning\lmsspace;
use enrol_lifelonglearning\local\caos\schema\role_enum;
use enrol_lifelonglearning\local\caos\schema\enrollment_event;
use enrol_lifelonglearning\local\caos\schema\schema_exception;



defined('MOODLE_INTERNAL') || die();

/**
 * Lifelong learning enrolment plugin
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class enrol_lifelonglearning_plugin extends enrol_plugin {

    /**
     * Returns localised name of enrol instance
     *
     * @param stdClass $instance (null is accepted too)
     * @return string
     */
     public function get_instance_name($instance) {
        global $DB;

        // We could show the csis space id here to differentiate different instances, but that
        // doesn't seem necessary in most cases.
        return parent::get_instance_name($instance);
    }

    /**
     * Returns link to page which may be used to add new instance of enrolment plugin in course.
     * @param int $courseid
     * @return moodle_url page url
     */
    public function get_newinstance_link($courseid) {
        $context = context_course::instance($courseid, MUST_EXIST);
        if (!has_capability('moodle/course:enrolconfig', $context) or !has_capability('enrol/lifelonglearning:config', $context)) {
            return NULL;
        }
        // multiple instances supported - multiple spaceids linked
        return new moodle_url('/enrol/lifelonglearning/addinstance.php', array('id'=>$courseid));
    }

    /**
     * Adds navigation links into course admin block.
     *
     * By defaults looks for manage links only.
     *
     * @param navigation_node $instancesnode
     * @param object $instance
     * @return void
     */
    public function add_course_navigation($instancesnode, stdClass $instance) {
        if ($instance->enrol !== 'lifelonglearning') {
            throw new coding_exception('invalid enrol instance!');
        }
        $context = context_course::instance($instance->courseid);
        if (has_capability('moodle/course:enrolreview', $context)) {
            $statuslink = new moodle_url('/enrol/lifelonglearning/status.php', array('id'=>$instance->id));
            $instancesnode->add($this->get_instance_name($instance), $statuslink, navigation_node::TYPE_SETTING);
        }
    }

    /**
     * Returns edit icons for the page with list of instances
     * @param stdClass $instance
     * @return array
     */
    public function get_action_icons(stdClass $instance) {
        global $OUTPUT;

        if ($instance->enrol !== 'lifelonglearning') {
            throw new coding_exception('invalid enrol instance!');
        }
        $icons = array();

        $statuslink = new moodle_url("/enrol/lifelonglearning/status.php", array('id'=>$instance->id));
        $icons[] = $OUTPUT->action_icon($statuslink, new pix_icon('t/preview', get_string('status'), 'core',
                    array('class' => 'iconsmall')));

        return $icons;
    }

    /**
     * Does this plugin allow manual unenrolment of a specific user?
     * Yes, but only if user suspended...
     *
     * @param stdClass $instance course enrol instance
     * @param stdClass $ue record from user_enrolments table
     *
     * @return bool - true means user with 'enrol/xxx:unenrol' may unenrol this user, false means nobody may touch this user enrolment
     */
    public function allow_unenrol_user(stdClass $instance, stdClass $ue) {
        if ($ue->status == ENROL_USER_SUSPENDED) {
            return true;
        }

        return false;
    }

    /**
     * Gets an array of the user enrolment actions
     *
     * @param course_enrolment_manager $manager
     * @param stdClass $ue A user enrolment object
     * @return array An array of user_enrolment_actions
     */
    public function get_user_enrolment_actions(course_enrolment_manager $manager, $ue) {
        $actions = array();
        $context = $manager->get_context();
        $instance = $ue->enrolmentinstance;
        $params = $manager->get_moodlepage()->url->params();
        $params['ue'] = $ue->id;
        if ($this->allow_unenrol_user($instance, $ue) && has_capability('enrol/lifelonglearning:unenrol', $context)) {
            $url = new moodle_url('/enrol/unenroluser.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/delete', ''), get_string('unenrol', 'enrol'), $url, array('class'=>'unenrollink', 'rel'=>$ue->id));
        }
        return $actions;
    }

    /**
     * Is it possible to delete enrol instance via standard UI?
     *
     * @param stdClass $instance
     * @return bool
     */
    public function can_delete_instance($instance) {
        $context = context_course::instance($instance->courseid);
        return has_capability('enrol/lifelonglearning:config', $context);
    }

    /**
     * Is it possible to hide/show enrol instance via standard UI?
     *
     * @param stdClass $instance
     * @return bool
     */
    public function can_hide_show_instance($instance) {
        $context = context_course::instance($instance->courseid);
        return has_capability('enrol/lifelonglearning:config', $context);
    }

    /**
     * Return information for enrolment instance containing list of parameters required
     * for enrolment, name of enrolment plugin etc.
     *
     * @param stdClass $instance enrolment instance
     * @return stdClass instance info.
     */
    public function get_enrol_info(stdClass $instance) {

        $instanceinfo = new stdClass();
        $instanceinfo->id = $instance->id;
        $instanceinfo->courseid = $instance->courseid;
        $instanceinfo->type = $this->get_name();
        $instanceinfo->name = $this->get_instance_name($instance);
        $instanceinfo->status = $this->can_self_enrol($instance);

        if ($instance->password) {
            $instanceinfo->requiredparam = new stdClass();
            $instanceinfo->requiredparam->enrolpassword = get_string('password', 'enrol_self');
        }

        // If enrolment is possible and password is required then return ws function name to get more information.
        if ((true === $instanceinfo->status) && $instance->password) {
            $instanceinfo->wsfunction = 'enrol_self_get_instance_info';
        }
        return $instanceinfo;
    }

    /**
     * Sync enrollments for all spaces in an lmstarget.
     *
     * @param progress_trace $trace
     * @param string $lmstarget
     * @param caos_datasource_interface $caos
     * @param uds_datasource_interface $uds
     */
    public function sync_all_spaces(progress_trace $trace, $lmstarget, caos_datasource_interface $caos, uds_datasource_interface $uds) {
        $spaces = $caos->getSpaces($lmstarget);
        foreach ($spaces as $space) {
            try {
                $trace->output("Syncing space $lmstarget / $space->id");
                $lmsspace = lmsspace::create_space($space);
                $this->sync_space_enrollment($trace, $lmsspace, $caos, $uds);
            } catch (\Exception $e) {
                $trace->output($e->getMessage());
                // Continue processing other spaces.
            }
        }
    }

    /**
     * Get configured lmstargets.
     *
     * @return string[]
     */
    public function get_lmstargets() {
        $lmstargets = $this->get_config('lmstarget', '');
        if (empty($lmstargets)) {
            return array();
        }
        return array_map('trim', explode(',', $lmstargets));
    }

    /**
     * Sync enrollment for a single lms space.
     *
     * @param progress_trace $trace
     * @param lmsspace $space
     * @param caos_datasource_interface $caos
     * @param uds_datasource_interface $uds
     * @param boolean $fullsync  Suspend enrollments which are missing in CAOS
     * @throws \moodle_exception
     */
    public function sync_space_enrollment(progress_trace $trace, lmsspace $space, caos_datasource_interface $caos, uds_datasource_interface $uds, $fullsync = false) {
        global $DB, $CFG;

        require_once($CFG->dirroot.'/local/wiscservices/locallib.php');

        $now = time();

        $instance = $space->get_enrol_instance();
        $course = $DB->get_record('course', array('id'=>$instance->courseid), '*', MUST_EXIST);
        $context = context_course::instance($course->id);

        // Load our configured roleid's
        $studentroleid = get_config('enrol_lifelonglearning', 'studentrole');
        $ferpastudentroleid = get_config('enrol_lifelonglearning', 'ferpastudentrole');
        $teacherroleid = get_config('enrol_lifelonglearning', 'teacherrole');

        // Query caos enrollments.
        $lmstargets = $this->get_lmstargets();

        $roster = $caos->getRosters($space->get_lmstarget(), array($space->get_spaceid()));
        $roster = reset($roster);
        if (!$roster) {
            $trace->output('Error fetching roster for spaceid \''. $space->get_spaceid() . '\'');
            return;
        }
        if ($roster->spaceId != $space->get_spaceid()) {
            throw new \moodle_exception('errorqueryingroster', 'enrol_lifelonglearning');
        }
        $wisc = new \local_wiscservices_plugin($trace);
        $wisc->set_datasource($uds);

        $roleids = array_unique(array($studentroleid, $ferpastudentroleid, $teacherroleid));
        // Remove any empty role ids.  This happens if we're configured to not sync a role.
        $roleids = array_filter($roleids, function($a){return !empty($a);});

        $active = array();
        $allpvihash = array();
        foreach ($roleids as $roleid) {
            $active[$roleid] = array();
        }

        foreach ($roster->enrollments as $enrollment) {
            try {
                $pvi = $enrollment->user->pvi;
                $allpvihash[$pvi] = true;
                if ($enrollment->active) {
                    $user = $wisc->get_user_by_pvi($pvi, false, true);
                    if (!$user) {
                        $trace->output('No UDS or Moodle user for PVI '. $pvi);
                        continue;
                    }
                    profile_load_custom_fields($user);
                    $ferpaflag = !empty($user->profile['ferpaName']);
                    switch ($enrollment->role) {
                        case role_enum::LEARNER:
                            if ($ferpaflag) {
                                $role = $ferpastudentroleid;
                            } else {
                                $role = $studentroleid;
                            }
                            break;
                        case role_enum::INSTRUCTOR:
                            $role = $teacherroleid;
                            break;
                        default:
                            throw new \moodle_exception('unknownrole', 'enrol_lifelonglearning');
                    }
                    if (!$role) {
                        // Configured to ignore this role.
                        continue;
                    }
                    $active[$role][] = $user->id;
                }
            } catch (\Exception $e) {
                $trace->output($e->getMessage());
                // Continue processing other users.
            }
        }

        $allactive = array();
        foreach ($active as $users) {
            $allactive = array_merge($allactive, $users);
        }
        foreach ($roleids as $roleid) {
            // Query all active course enrollments for role.
            $sql= "SELECT ue.userid
                     FROM {user_enrolments} ue
                     JOIN {role_assignments} ra ON (ra.itemid = ue.enrolid AND ra.userid = ue.userid AND ra.component = 'enrol_lifelonglearning')
                    WHERE ue.enrolid = ? AND ue.status = ? AND ra.roleid = ?";
            $enrollments = $DB->get_records_sql($sql, array($instance->id, ENROL_USER_ACTIVE, $roleid));
            $enrollments = array_keys($enrollments);

            // Users to enrol
            $toadd = array_diff($active[$roleid], $enrollments);
            // Users to suspend.  We either require an inactive record in the roster, or an enrollment (active or inactive)
            // in another role.  This to account for people changing roles (e.g. ferpastudent to student).
            $tosuspend = array_diff($enrollments, $active[$roleid]);

            // Process adds.
            foreach ($toadd as $userid) {
                $trace->output("Enrolling user $userid");
                $this->enrol_user($instance, $userid, $roleid, 0, 0, ENROL_USER_ACTIVE, true);
            }

            // Process suspends.
            $userpvis = $DB->get_records_list('user', 'id', $tosuspend, '', 'id,idnumber');
            foreach ($userpvis as $userid => $user) {
                $pvi = $user->idnumber;
                if (!empty($allpvihash[$pvi]) || $fullsync) {
                    // Suspend action.  Typical moodle external enrol plugins support many different options here depending on
                    // site config.  For UW Moodle we've settled on "Suspend enrollment + remove all roles", so this class
                    // is hard-coded to use that method.

                    $trace->output("Suspending user $userid");

                    // Remove role.
                    role_unassign($roleid, $userid, $context->id, 'enrol_lifelonglearning', $instance->id);

                    // The user might be active in a different role, so better to check before suspending.
                    if (in_array($userid, $allactive)) {
                        $status = ENROL_USER_ACTIVE;
                    } else {
                        $status = ENROL_USER_SUSPENDED;
                    }
                    // Suspend enrollment if appropriate.
                    $this->enrol_user($instance, $userid, 0, 0, 0, $status);
                } else {
                    // User vanished from the roster.
                    //
                    // If this was the timetable (CHUB) side, there would be all sorts of complicated logic to deal with extraneous
                    // enrollments (e.g. enrollments that have vanished upstream.)
                    // However, since lifelong learning is a new system, I'm hoping we won't have enrollments mysteriously
                    // vanish.  Here we just log and move on.
                    //
                    // It is possible that the PVI changed, and we have the user under a different PVI.  In that case,
                    // we will eventually update and the user will be removed later.
                    $trace->output("No L3 enrollment for $userid in space '".$instance->{lmsspace::SPACEID_FIELD}."'");

                }
            }
        }

        // Finally look for any role assignments we're not configured to handle and clean them up.  This will happen if the
        // role mapping is changed in the config.
        list ($roleidsql, $params) = $DB->get_in_or_equal($roleids, SQL_PARAMS_NAMED, 'role', false, '-1');
        $sql= "SELECT ra.*
                     FROM {role_assignments} ra
                     JOIN {user_enrolments} ue ON (ra.itemid = ue.enrolid AND ra.userid = ue.userid AND ra.component = 'enrol_lifelonglearning')
                    WHERE ue.enrolid = :enrolid AND ra.roleid $roleidsql";
        $params['enrolid'] = $instance->id;
        $assignments = $DB->get_records_sql($sql, $params);
        foreach ($assignments as $assignment) {
            $trace->output("Removing user $assignment->userid from bogus role");
            role_unassign($assignment->roleid, $assignment->userid, $context->id, 'enrol_lifelonglearning', $instance->id);
            if (!in_array($assignment->userid, $allactive)) {
                $trace->output("Suspending user $assignment->userid");
                $this->enrol_user($instance, $assignment->userid, 0, 0, 0, ENROL_USER_SUSPENDED);
            }
        }
        if (!empty($toadd) || !empty($tosuspend)) {
            $space->set_time_rosterupdate($now);
        }
        $space->set_time_fullsync($now);
    }

    /**
     * Process a single enrollment update in a space.
     *
     * @param progress_trace $trace
     * @param lmsspace $space
     * @param enrollment $enrollment
     * @param uds_datasource_interface $uds
     * @return bool true on success
     * @throws exception
     */
    public function process_enrollment_event(progress_trace $trace, enrollment_event $event,
            $lmsTarget, uds_datasource_interface $uds) {

        global $DB;

        $result = true;

        if (!in_array($lmsTarget, $this->get_lmstargets())) {
            throw new \moodle_exception('unknownlmstarget', 'enrol_lifelonglearning');
        }

        // Lookup lms space.
        $lmsspace = lmsspace::load_space_by_id($event->spaceId, $lmsTarget);
        if (!$lmsspace) {
            // No such space.
            $trace->output("No such space.");
            return false;
        }
        $instance = $lmsspace->get_enrol_instance();
        $context = \context_course::instance($instance->courseid);

        $wisc = new \local_wiscservices_plugin($trace);
        $wisc->set_datasource($uds);

        // Always query UDS if this is an enrollment event
        // Do not allow missing netid.  (UWMOODLE-1374)
        $doupdate = $event->active;
        $user = $wisc->get_user_by_pvi($event->user->pvi, $doupdate, true);
        if (!$user) {
            $trace->output("Unable to create moodle user for '".$event->user->pvi."'.");
            return false;
        }

        profile_load_custom_fields($user);
        $ferpaflag = !empty($user->profile->ferpaName);

        if ($event->active) {
            // Enrol or unsuspend user.
            $courseurl = new moodle_url('/course/view.php', array('id'=>$instance->courseid));
            $trace->output("Enrolling ".$event->role." '".$event->user->pvi."' to space ".$courseurl);
            switch ($event->role) {
                case role_enum::INSTRUCTOR:
                    $this->enrol_user($instance, $user->id, $this->get_config('teacherrole'), 0, 0, ENROL_USER_ACTIVE, true);
                    break;
                case role_enum::LEARNER:
                    // Check ferpa flags.
                    if ($ferpaflag) {
                        $this->enrol_user($instance, $user->id, $this->get_config('ferpastudentrole'), 0, 0, ENROL_USER_ACTIVE, true);
                    } else {
                        $this->enrol_user($instance, $user->id, $this->get_config('studentrole'), 0, 0, ENROL_USER_ACTIVE, true);
                    }
                    break;
                default:
                    throw new schema_exception('unknownrole', 'enrol_lifelonglearning');
            }

        } else {
            // Remove roles.
            $courseurl = new moodle_url('/course/view.php', array('id'=>$instance->courseid));
            $trace->output("Unenrolling ".$event->role." '".$event->user->pvi."' from space ".$courseurl);
            switch ($event->role) {
                case role_enum::INSTRUCTOR:
                    role_unassign($this->get_config('teacherrole'), $user->id, $context->id, 'enrol_lifelonglearning', $instance->id);
                    break;
                case role_enum::LEARNER:
                    // Remove all student roles in case the ferpa flag changed recently.
                    role_unassign($this->get_config('ferpastudentrole'), $user->id, $context->id, 'enrol_lifelonglearning', $instance->id);
                    role_unassign($this->get_config('studentrole'), $user->id, $context->id, 'enrol_lifelonglearning', $instance->id);
                    break;
                default:
                    throw new schema_exception('unknownrole', 'enrol_lifelonglearning');
            }

            // The user might be active in a different role, so better to check before suspending.
            // Suspend enrollment if appropriate.
            $enrolled = $DB->record_exists('user_enrolments', array('enrolid'=>$instance->id, 'userid'=>$user->id));
            $stillactive = $DB->record_exists('role_assignments', array('userid'=>$user->id, 'component'=>'enrol_lifelonglearning', 'itemid'=>$instance->id));
            if (!$stillactive && $enrolled) {
                $trace->output("Suspending enrollment.");
                $this->enrol_user($instance, $user->id, 0, 0, 0, ENROL_USER_SUSPENDED);
            } else if ($stillactive) {
                $trace->output("Maintaining enrollment due to other role assignments.");
            } else {
                $trace->output("No such enrollment.");
            }
        }

        $lmsspace->set_time_rosterupdate(time());
        return $result;

    }

}
