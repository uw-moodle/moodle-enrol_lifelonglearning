<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lifelonglearning enrolment external API functions.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_lifelonglearning\lmsspace;
use enrol_lifelonglearning\local\caos\schema\schema_exception;
use enrol_lifelonglearning\local\caos\schema\space;
use enrol_lifelonglearning\local\caos\schema\enrollment_event;

use local_wiscservices\local\uds\uds_datasource;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/externallib.php');

class enrol_lifelonglearning_external extends \external_api {

    /**
     * Definition of handle_event parameters.
     */
    public static function handle_event_parameters() {
        return new external_function_parameters(
            array('event' => new \external_single_structure(
                array(
                    'lmsTarget' => new \external_value(PARAM_RAW, 'XML message'),
                    'messages' => new \external_multiple_structure(new \external_value(PARAM_RAW, 'XML message')),
                )
            ))
        );
    }

    /**
     * Definition of handle_event returns.
     */
    public static function handle_event_returns() {
        return  new \external_value(PARAM_RAW, 'result');
    }

    /**
     * Roster change event handler.
     *
     * This call produces no return status, but will throw an exception (SOAP error, etc) on a fatal error. Errors
     * such as a missing user upstream do not generate an error.  Transient errors such as a datasource connection failure generate
     * moodle exceptions.
     */
    public static function handle_event($params, \enrol_lifelonglearning_plugin $enrolplugin = null) {
        global $DB;

        $params = self::validate_parameters(self::handle_event_parameters(), array('event'=>$params));
        $params = $params['event'];

        // Check access.
        require_capability('enrol/lifelonglearning:triggerevent', \context_system::instance());

        if (!enrol_is_enabled('lifelonglearning')) {
            throw new \moodle_exception('enrolnotenabled', 'enrol_lifelonglearning');
        }

        // Trace to a buffer, for return
        $trace = new \progress_trace_buffer(new \text_progress_trace(), false);
        $uds = new uds_datasource();

        if (!$enrolplugin) {
            /* @var $enrolplugin enrol_lifelonglearning_plugin */
            $enrolplugin = enrol_get_plugin('lifelonglearning');
        }

        $lmstargets = $enrolplugin->get_lmstargets();
        if (!in_array($params['lmsTarget'], $lmstargets)) {
            throw new \moodle_exception('unknownlmstarget', 'enrol_lifelonglearning');
        }
        $status = "OK";

        foreach ($params['messages'] as $message) {

            $xml = simplexml_load_string($message);
            if ($xml === false) {
                throw new schema_exception('Invalid XML');
            }

            switch ($xml->getName()) {
                case 'enrollment':
                    $enrollmentevent = new enrollment_event($xml);

                    // Process the enrollment.
                    $result = $enrolplugin->process_enrollment_event($trace, $enrollmentevent, $params['lmsTarget'], $uds);
                    if (!$result) {
                        $status = "WARN";
                    }
                    break;

                case 'space':
                    $space = new space($xml);
                    if ($space->lmsTarget != $params['lmsTarget']) {
                        throw new moodle_exception('Wrong lmsTarget');
                    }

                    // See if the space already exists.
                    if ($lmsspace = lmsspace::load_space($space)) {
                        $trace->output('Space already exists.');
                        $status = "WARN";
                    } else {
                        $trace->output('New space.');
                    }

                    // Create the space.
                    $lmsspace = lmsspace::create_space($space);
                    $trace->output('Space URL: '.new moodle_url('/course/view.php', array('id'=>$lmsspace->get_enrol_instance()->courseid)));

                    break;

                default:
                    throw new schema_exception('Unknown message: '.$xml->getName());
            }

        }
        $trace->finished();
        return "$status : " . $trace->get_buffer();
    }
}