<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mock datasource for lifelonglearning enrolment tests.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_lifelonglearning\local\caos\datasource;
use enrol_lifelonglearning\local\caos\schema\space;
use enrol_lifelonglearning\local\caos\schema\roster;

defined('MOODLE_INTERNAL') || die();

global $CFG;

class enrol_lifelonglearning_caos_datasource_mock implements datasource {

    protected $timestep = 0;

    /**
     * For testing, which version of roster.
     * @param unknown $timestep
     */
    public function set_timestep($timestep) {
        $this->timestep = $timestep;
    }

    /* (non-PHPdoc)
     * @see \enrol_lifelonglearning\local\caos\datasource::getRosters()
     */
    public function getRosters($lmstarget, array $spaceids) {
        // Space 1
        $rosters = array();
        $enrollments = array();
        $enrollments[] =
                array ( 'role'      => 'Learner',
                        'active'    => true,
                        'user'      => array('pvi' => 'PVI1'),
                );
        $enrollments[] =
                array ( 'role'      => 'Learner',
                        'active'    => true,
                        'user'      => array('pvi' => 'PVI2'),
                );
        $enrollments[] =
                array ( 'role'      => 'Instructor',
                        'active'    => true,
                        'user'      => array('pvi' => 'PVI6'),
                );

        // Changes over time.

        if ($this->timestep >= 1) {
            // student1 active = false
            $enrollments[0]['active'] = false;
        }
        if ($this->timestep >= 2) {
            // student1 active = true
            // student2 active = false
            $enrollments[0]['active'] = true;
            $enrollments[1]['active'] = false;
        }
        if ($this->timestep >= 3) {
            // add an inactive teacher
            $enrollments[] =
                array ( 'role'      => 'Instructor',
                        'active'    => false,
                        'user'      => array('pvi' => 'PVI7'),
                );
        }
        if ($this->timestep >= 4) {
            // add an active student
            $enrollments[] =
                    array ( 'role'      => 'Learner',
                            'active'    => true,
                            'user'      => array('pvi' => 'PVI3'),
                    );
        }
        if ($this->timestep >= 5) {
            // student1 vanishes
            unset($enrollments[0]);
        }

        $rosters['Space1'] = new roster(
                array(  'spaceId'       => 'Space1',
                        'enrollments'   => $enrollments,
                ));

        // Space 2
        $enrollments = array();
        $enrollments[] =
                array ( 'role'      => 'Instructor',
                        'active'    => true,
                        'user'      => array('pvi' => 'PVI6'),
                );
        $rosters['Space2'] = new roster(
                array(  'spaceId'       => 'Space2',
                        'enrollments'   => $enrollments,
                ));

        // Space 3
        $enrollments = array();
        $enrollments[] =
        array ( 'role'      => 'Learner',
                'active'    => true,
                'user'      => array('pvi' => 'PVI1'),
        );
        $enrollments[] =
        array ( 'role'      => 'Learner',
                'active'    => true,
                'user'      => array('pvi' => 'PVI2'),
        );
        $enrollments[] =
        array ( 'role'      => 'Learner',
                'active'    => true,
                'user'      => array('pvi' => 'PVI3'),
        );
        $enrollments[] =
        array ( 'role'      => 'Learner',
                'active'    => true,
                'user'      => array('pvi' => 'PVI4'), // ferpa student
        );
        $enrollments[] =
        array ( 'role'      => 'Learner',
                'active'    => true,
                'user'      => array('pvi' => 'PVI5'), // ferpa student
        );
        $enrollments[] =
        array ( 'role'      => 'Instructor',
                'active'    => true,
                'user'      => array('pvi' => 'PVI6'),
        );

        // Changes over time.

        if ($this->timestep >= 1) {
            // non ferpa students drop
            $enrollments[0]['active'] = false;
            $enrollments[1]['active'] = false;
            $enrollments[2]['active'] = false;
        }

        if ($this->timestep >= 2) {
            // non ferpa students add, one ferpa student drops
            $enrollments[0]['active'] = true;
            $enrollments[1]['active'] = true;
            $enrollments[2]['active'] = true;
            $enrollments[3]['active'] = false;
            $enrollments[4]['active'] = true;
        }

        $rosters['Space3'] = new roster(
                array(  'spaceId'       => 'Space3',
                        'enrollments'   => $enrollments,
                ));

        // Return only requested spaces
        return array_intersect_key($rosters, array_flip($spaceids));
    }

    /* (non-PHPdoc)
     * @see \enrol_lifelonglearning\local\caos\datasource::getSpaces()
     */
    public function getSpaces($lmstarget) {
        if ($lmstarget != "Moodle") {
            return array();
        }
        $spaces = array();
        $spaces[] = new space (
                array(  'id'                  =>'Space1',
                        'lmsTarget'           =>'Moodle',
                        'category'            =>
                            array(
                                'id' => 'Cat1',
                                'description'=>'Lifelong learning'
                            ),
                        'subcategory'         =>
                            array(
                                'id' => 'Sub1',
                                'description'=>'Test courses'
                            ),
                        'spaceProvisioning' =>
                            array(
                                'title'               => 'Test course 1',
                                'startdate'           => '2015-09-01',
                                'enddate'             => '2015-12-31',
                            )
                        ));
        $spaces[] = new space (
                array(  'id'                  =>'Space2',
                        'lmsTarget'           =>'Moodle',
                        'category'            =>
                        array(
                                'id' => 'Cat1',
                                'description'=>'Lifelong learning'
                        ),
                        'subcategory'         =>
                        array(
                                'id' => 'Sub2',
                                'description'=>'More courses'
                        ),
                        'spaceProvisioning' =>
                        array(
                                'title'               => 'Test course 2',
                                'startdate'           => '2015-09-01',
                                'enddate'             => '2015-12-31',
                        )
                ));
        $spaces[] = new space (
                array(  'id'                  =>'Space3',
                        'lmsTarget'           =>'Moodle',
                        'spaceProvisioning' =>
                        array(
                                'title'               => 'Ferpa test course 1',
                        )
                ));

        return $spaces;
    }

}
