<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests for lmsspace class.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_lifelonglearning\local\caos\schema\space;
use enrol_lifelonglearning\local\caos\schema\category;
use enrol_lifelonglearning\local\caos\schema\subcategory;


use enrol_lifelonglearning\lmsspace;

defined('MOODLE_INTERNAL') || die();

/**
 * Tests for lmsspace class.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group      enrol_lifelonglearning
 */

class enrol_lifelonglearning_lmsspace_test extends advanced_testcase {

    public function setUp() {
        $this->resetAfterTest(true);
    }

    protected function generate_space() {
        static $n = 0;
        $n++;

        return new space(
            array(  'id'                  =>"Space$n",
                    'lmsTarget'           =>'Moodle1',
                    'category'            =>
                    array(
                            'id' => "Category$n",
                            'description'=>"Description of category$n",
                    ),
                    'subcategory'         =>
                    array(
                            'id' => "SubCategory$n",
                            'description'=>"Description of subcategory$n",
                    ),
                    'spaceProvisioning' =>
                    array(
                            'title'               => "Test space $n",
                            'startDate'           => '2015-09-01',
                            'endDate'             => '2015-09-30',
                    )
            ));

    }

    public function test_create_space() {
        global $DB;
        $space1 = $this->generate_space();

        // Creating in existing course.
        $course1 = $this->getDataGenerator()->create_course();
        $lmsspace1 = lmsspace::create_space($space1, $course1);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace1);
        $this->assertEquals(1, $DB->count_records('enrol', array('enrol' => 'lifelonglearning')));
        $this->assertEquals(2, $DB->count_records('course'));
        $this->assertEquals($space1->id, $lmsspace1->get_spaceid());
        $this->assertEquals($space1->lmsTarget, $lmsspace1->get_lmstarget());

        // Adding again shouldn't make a new space.
        $lmsspace2 = lmsspace::create_space($space1, $course1);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace2);
        $this->assertEquals($lmsspace1->get_enrol_instance(), $lmsspace2->get_enrol_instance());
        $this->assertEquals(1, $DB->count_records('enrol', array('enrol' => 'lifelonglearning')));

        // Adding again shouldn't make a new space, without course specified.
        $lmsspace2 = lmsspace::create_space($space1);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace2);
        $this->assertEquals($lmsspace1->get_enrol_instance(), $lmsspace2->get_enrol_instance());
        $this->assertEquals(1, $DB->count_records('enrol', array('enrol' => 'lifelonglearning')));

        // Adding a second space to the same course.
        $space2 = $this->generate_space();
        $lmsspace2 = lmsspace::create_space($space2, $course1);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace2);
        $this->assertEquals($space2->id, $lmsspace2->get_spaceid());
        $this->assertEquals(2, $DB->count_records('enrol', array('enrol' => 'lifelonglearning')));
        $this->assertEquals(2, $DB->count_records('course'));

        // Creating a space in a new course.
        $space3 = $this->generate_space();
        $lmsspace3 = lmsspace::create_space($space3);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace1);
        $this->assertEquals(3, $DB->count_records('enrol', array('enrol' => 'lifelonglearning')));
        $this->assertEquals(3, $DB->count_records('course'));
        $this->assertEquals($space3->id, $lmsspace3->get_spaceid());
        $this->assertEquals($space3->lmsTarget, $lmsspace3->get_lmstarget());
    }

    public function test_load_space() {
        $space = $this->generate_space();

        $course1 = $this->getDataGenerator()->create_course();
        $lmsspace1 = lmsspace::create_space($space, $course1);
        $lmsspace2 = lmsspace::load_space($space);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace1);
        $this->assertEquals($lmsspace1->get_enrol_instance(), $lmsspace2->get_enrol_instance());

        $lmsspace2 = lmsspace::load_space($space);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace1);
        $this->assertEquals($lmsspace1->get_enrol_instance(), $lmsspace2->get_enrol_instance());

        $lmsspace3 = lmsspace::load_space_by_id($space->id, $space->lmsTarget);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace1);

        $lmsspace4 = lmsspace::load_space_by_id("NoSuchSpace", $space->lmsTarget);
        $this->assertNull($lmsspace4);
    }

    public function test_update_space() {
        $space = $this->generate_space();
        $course1 = $this->getDataGenerator()->create_course();
        $lmsspace1 = lmsspace::create_space($space, $course1);

        $space->category = new category(array('id'=>"NEWCategory1", 'description'=>"NEWCategory1"));
        $space->subcategory = new subcategory(array('id'=>"NEWSubCategory1", 'description'=>"NEWSubCategory1"));;
        $space->status = "Inactive";
        $lmsspace1->update($space);

        $lmsspace2 = lmsspace::load_space($space);
        $this->assertInstanceOf('\enrol_lifelonglearning\lmsspace', $lmsspace1);
        $this->assertEquals($lmsspace1->get_enrol_instance(), $lmsspace2->get_enrol_instance());
        $enroltext = implode(' ', (array)$lmsspace2->get_enrol_instance());
        // Ensure there are two "NEW" strings in enrol record.
        $this->assertEquals(2, substr_count($enroltext, 'NEW'));
    }

    public function test_update_space_exception() {

        $space = $this->generate_space();
        $course1 = $this->getDataGenerator()->create_course();
        $lmsspace1 = lmsspace::create_space($space, $course1);

        // Try an illegal update
        $space->id = "NewSpace1";
        $space->lmsTarget = "NewMoodle1";
        $this->setExpectedException('moodle_exception');
        $lmsspace1->update($space);
    }

    public function test_categoryid_to_spaceid() {
        $this->assertEquals('CAT', lmsspace::categoryid_to_spaceid('LLL-CAT'));
        $this->assertNull(lmsspace::categoryid_to_spaceid('CAT'));
        $this->assertNull(lmsspace::categoryid_to_spaceid('CAT-LLL-CAT'));
    }

    public function test_spaceid_to_categoryid() {
        $this->assertEquals('LLL-CAT', lmsspace::generate_categoryid('CAT'));
    }

    public function test_get_moodle_category() {
        global $DB;
        $space1 = $this->generate_space();

        $numcats = $DB->count_records('course_categories');

        // Check course categories.
        enrol_lifelonglearning_lmsspace_mock::mock_get_moodle_category($space1);
        $numcats += 2;
        $this->assertEquals($numcats, $DB->count_records('course_categories'));
        $parentcat = $DB->get_record('course_categories', array('idnumber' => 'LLL-'.$space1->category->id));
        $this->assertNotEmpty($parentcat);
        $subcat = $DB->get_record('course_categories', array('idnumber' => 'LLL-'.$space1->category->id.'/'.$space1->subcategory->id));
        $this->assertNotEmpty($subcat);
        $this->assertEquals($parentcat->id, $subcat->parent);

        // Check reusing both categories
        $space2 = $this->generate_space();
        $space2->category = $space1->category;
        $space2->subcategory = $space1->subcategory;
        enrol_lifelonglearning_lmsspace_mock::mock_get_moodle_category($space2);
        $space2->category = $space2->category;
        $space2->subcategory = $space2->subcategory;
        $numcats += 0;
        $this->assertEquals($numcats, $DB->count_records('course_categories'));

        // Check reusing subcategory
        $space3 = $this->generate_space();
        $space3->category = $space1->category;
        enrol_lifelonglearning_lmsspace_mock::mock_get_moodle_category($space3);
        $numcats += 1;
        $this->assertEquals($numcats, $DB->count_records('course_categories'));
        $this->assertNotEmpty($parentcat);
        $subcat = $DB->get_record('course_categories', array('idnumber' => 'LLL-'.$space3->category->id.'/'.$space3->subcategory->id));
        $this->assertNotEmpty($subcat);
        $this->assertEquals($parentcat->id, $subcat->parent);

        // Check with new categories
        $space4 = $this->generate_space();
        enrol_lifelonglearning_lmsspace_mock::mock_get_moodle_category($space4);
        $numcats += 2;
        $this->assertEquals($numcats, $DB->count_records('course_categories'));

        // Check with new categories, same subcategory id.  This should result in two new categories.
        $space5 = $this->generate_space();
        $space5->subcategory = $space1->subcategory;
        enrol_lifelonglearning_lmsspace_mock::mock_get_moodle_category($space5);
        $numcats += 2;
        $this->assertEquals($numcats, $DB->count_records('course_categories'));
    }

    public function test_create_moodle_course() {
        global $DB;
        $space1 = $this->generate_space();

        // Create course for space1
        enrol_lifelonglearning_lmsspace_mock::mock_create_moodle_course($space1);
        $this->assertEquals(2, $DB->count_records('course'));
        $course1 = $DB->get_record('course', array('shortname'=>lmsspace::generate_default_shortname($space1)));
        $this->assertNotEmpty($course1);
        $this->assertEquals($space1->spaceProvisioning->title, $course1->fullname);
        $this->assertEquals(strtotime('2015-08-30'), $course1->startdate);
        $numsections = $DB->get_field('course_format_options', 'value',
                array('courseid'=>$course1->id, 'format'=>'weeks', 'name'=>'numsections'));
        $this->assertEquals(5, $numsections);

        $subcat = $DB->get_record('course_categories', array('idnumber' =>
                'LLL-'.$space1->category->id.'/'.$space1->subcategory->id));
        $this->assertNotEmpty($subcat);
        $this->assertEquals($subcat->id, $course1->category);

        // Creating a new course with the same shortname.
        enrol_lifelonglearning_lmsspace_mock::mock_create_moodle_course($space1);
        $this->assertEquals(3, $DB->count_records('course'));

        // Creating new course with template.
        $enrol = enrol_get_plugin('lifelonglearning');
        $enrol->set_config('template', $course1->shortname);
        $DB->set_field('course', 'summary', 'SUMMARY', array('id'=>$course1->id));
        $space2 = $this->generate_space();
        $course2 = enrol_lifelonglearning_lmsspace_mock::mock_create_moodle_course($space2);
        $this->assertEquals('SUMMARY', $course2->summary);
        $this->assertEquals(lmsspace::generate_default_shortname($space2), $course2->shortname);
        $this->assertEquals($space2->spaceProvisioning->title, $course2->fullname);
    }
}

class enrol_lifelonglearning_lmsspace_mock extends lmsspace {
    public static function mock_get_moodle_category(space $space) {
        return self::get_moodle_category($space);
    }
    public static function mock_create_moodle_course(space $space) {
        return self::create_moodle_course($space);
    }
}