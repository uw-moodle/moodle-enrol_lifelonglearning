<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests for enum class.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Tests for enum class.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group      enrol_lifelonglearning
 */

class enrol_lifelonglearning_enum_test extends basic_testcase {
    public function test_isvalid() {
        $this->assertTrue(enrol_lifelonglearning_enum_test_class::isvalid('Apple', true));
        $this->assertTrue(enrol_lifelonglearning_enum_test_class::isvalid('D\'Anjou', true));
        $this->assertTrue(enrol_lifelonglearning_enum_test_class::isvalid('Kiwi', true));
        $this->assertFalse(enrol_lifelonglearning_enum_test_class::isvalid('kiwi', true));
        $this->assertFalse(enrol_lifelonglearning_enum_test_class::isvalid('hotdog', true));
        $this->assertTrue(enrol_lifelonglearning_enum_test_class::isvalid('0', false));
        $this->assertFalse(enrol_lifelonglearning_enum_test_class::isvalid('0', true));
        $this->assertTrue(enrol_lifelonglearning_enum_test_class::isvalid(0, true));
        $this->assertTrue(enrol_lifelonglearning_enum_test_class::isvalid(0, false));
    }
}

class enrol_lifelonglearning_enum_test_class extends \enrol_lifelonglearning\local\caos\schema\enum {
    const APPLE = 'Apple';
    const KIWI =  'Kiwi';
    const DANJOU = 'D\'Anjou';
    const ZERO = 0;
}