<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests for external api functions.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_lifelonglearning\local\caos\schema\space;
use enrol_lifelonglearning\local\caos\schema\enrollment_event;
use enrol_lifelonglearning\local\caos\schema\role_enum;

use local_wiscservices\local\uds\datasource;

use enrol_lifelonglearning\lmsspace;

global $CFG;

require_once($CFG->dirroot.'/enrol/lifelonglearning/externallib.php');
require_once('uds_datasource_mock.php');

defined('MOODLE_INTERNAL') || die();

/**
 * Tests for external api functions.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group      enrol_lifelonglearning
 */

class enrol_lifelonglearning_externallib_test extends advanced_testcase {

    public function setUp() {
        $this->resetAfterTest(true);

        // Set lmstarget
        set_config('lmstarget', 'Moodle', 'enrol_lifelonglearning');

        // Enable plugin
        $enabled = enrol_get_plugins(true);
        $enabled['lifelonglearning'] = true;
        $enabled = array_keys($enabled);
        set_config('enrol_plugins_enabled', implode(',', $enabled));

        // Become admin user
        $this->setAdminUser();
    }

    public function test_create_space() {
        global $DB;

        $xml = <<<'EOF'
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ns3:space xmlns="http://services.wisc.edu/l3-lms/enrollment"
    xmlns:ns2="http://services.wisc.edu/l3-lms/user" xmlns:ns3="http://services.wisc.edu/l3-lms/space">
    <ns3:id>1</ns3:id>
    <ns3:lmsTarget>Moodle</ns3:lmsTarget>
    <ns3:category>
        <ns3:id>1</ns3:id>
        <ns3:description>L3 Category</ns3:description>
    </ns3:category>
    <ns3:subcategory>
        <ns3:id>2</ns3:id>
        <ns3:description>L3 Subcategory</ns3:description>
    </ns3:subcategory>
    <ns3:spaceProvisioning>
        <ns3:title>Space1 Fullname</ns3:title>
        <ns3:shortTitle>Space1</ns3:shortTitle>
        <ns3:startDate>2015-09-01</ns3:startDate>
    </ns3:spaceProvisioning>
</ns3:space>
EOF;
        enrol_lifelonglearning_external::handle_event(array('lmsTarget' => 'Moodle', 'messages' => array($xml)));

        $space = lmsspace::load_space_by_id('1', 'Moodle');
        $this->assertNotNull($space);
        $this->assertEquals('1', $space->get_spaceid());
        $this->assertEquals('Space1 Fullname', $space->get_title());
        $this->assertEquals('L3 Category/L3 Subcategory', $space->get_categories());

        $xml = <<<'EOF'
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ns3:space xmlns="http://services.wisc.edu/l3-lms/enrollment"
    xmlns:ns2="http://services.wisc.edu/l3-lms/user" xmlns:ns3="http://services.wisc.edu/l3-lms/space">
    <ns3:id>2</ns3:id>
    <ns3:lmsTarget>Moodle</ns3:lmsTarget>
    <ns3:category>
    </ns3:category>
    <ns3:subcategory>
    </ns3:subcategory>
    <ns3:spaceProvisioning>
        <ns3:title>Space2 Fullname</ns3:title>
        <ns3:shortTitle>Space2</ns3:shortTitle>
        <ns3:startDate>2015-09-01</ns3:startDate>
    </ns3:spaceProvisioning>
</ns3:space>
EOF;
        enrol_lifelonglearning_external::handle_event(array('lmsTarget' => 'Moodle', 'messages' => array($xml)));

        $space = lmsspace::load_space_by_id('2', 'Moodle');
        $this->assertNotNull($space);
        $this->assertEquals('2', $space->get_spaceid());
        $this->assertEquals('Space2 Fullname', $space->get_title());
        $this->assertEquals('', $space->get_categories());

        // Similar test, but with different category xml.  Simplexml treats these cases slightly differently.

        $xml = <<<'EOF'
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ns3:space xmlns="http://services.wisc.edu/l3-lms/enrollment"
    xmlns:ns2="http://services.wisc.edu/l3-lms/user" xmlns:ns3="http://services.wisc.edu/l3-lms/space">
    <ns3:id>3</ns3:id>
    <ns3:lmsTarget>Moodle</ns3:lmsTarget>
    <ns3:category/>
    <ns3:subcategory/>
    <ns3:spaceProvisioning>
        <ns3:title>Space3 Fullname</ns3:title>
        <ns3:shortTitle>Space3</ns3:shortTitle>
        <ns3:startDate>2015-09-01</ns3:startDate>
    </ns3:spaceProvisioning>
</ns3:space>
EOF;
        enrol_lifelonglearning_external::handle_event(array('lmsTarget' => 'Moodle', 'messages' => array($xml)));

        $space = lmsspace::load_space_by_id('3', 'Moodle');
        $this->assertNotNull($space);
        $this->assertEquals('3', $space->get_spaceid());
        $this->assertEquals('Space3 Fullname', $space->get_title());
        $this->assertEquals('', $space->get_categories());
    }

    public function test_update_enrollment() {
        global $DB;

        // Test 1

        $observer = $this->getMockBuilder('enrol_lifelonglearning_plugin')
            ->setMethods(array('process_enrollment_event', 'get_lmstargets'))
            ->getMock();
        $observer->expects($this->any())
            ->method('get_lmstargets')
            ->will($this->returnValue(array('Moodle')));

        $xml = <<<'EOF'
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<enrollment xmlns="http://services.wisc.edu/l3-lms/enrollment"
xmlns:ns2="http://services.wisc.edu/l3-lms/user"
xmlns:ns3="http://services.wisc.edu/l3-lms/space">
        <spaceId>2</spaceId>
        <ns2:user>
                <ns2:pvi>100001</ns2:pvi>
        </ns2:user>
        <role>Learner</role>
        <active>true</active>
</enrollment>
EOF;

        $observer->expects($this->once())
                 ->method('process_enrollment_event')
                 ->with($this->isInstanceOf('progress_trace'),
                        $this->callback(function(enrollment_event $event) {
                             return $event->spaceId == 2
                                     && $event->user->pvi == '100001'
                                     && $event->role == role_enum::LEARNER
                                     && $event->active;
                        }),
                        $this->equalTo('Moodle'),
                        $this->anything());

        enrol_lifelonglearning_external::handle_event(array('lmsTarget' => 'Moodle', 'messages' => array($xml)), $observer);

        // Test 2

        $observer = $this->getMockBuilder('enrol_lifelonglearning_plugin')
            ->setMethods(array('process_enrollment_event', 'get_lmstargets'))
            ->getMock();
        $observer->expects($this->any())
            ->method('get_lmstargets')
            ->will($this->returnValue(array('Moodle')));

        $xml = <<<'EOF'
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<enrollment xmlns="http://services.wisc.edu/l3-lms/enrollment"
xmlns:ns2="http://services.wisc.edu/l3-lms/user"
xmlns:ns3="http://services.wisc.edu/l3-lms/space">
        <spaceId>2</spaceId>
        <ns2:user>
                <ns2:pvi>100001</ns2:pvi>
        </ns2:user>
        <role>Learner</role>
        <active>false</active>
</enrollment>
EOF;

        $observer->expects($this->once())
            ->method('process_enrollment_event')
            ->with($this->isInstanceOf('progress_trace'),
                $this->callback(function(enrollment_event $event) {
                    return $event->spaceId == 2
                            && $event->user->pvi == '100001'
                            && $event->role == role_enum::LEARNER
                            && !$event->active;
                }),
                $this->equalTo('Moodle'),
                $this->anything());

        enrol_lifelonglearning_external::handle_event(array('lmsTarget' => 'Moodle', 'messages' => array($xml)), $observer);

        // Test 3

        $observer = $this->getMockBuilder('enrol_lifelonglearning_plugin')
               ->setMethods(array('process_enrollment_event', 'get_lmstargets'))
            ->getMock();
        $observer->expects($this->any())
            ->method('get_lmstargets')
            ->will($this->returnValue(array('Moodle')));

        $xml = <<<'EOF'
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<enrollment xmlns="http://services.wisc.edu/l3-lms/enrollment"
xmlns:ns2="http://services.wisc.edu/l3-lms/user"
xmlns:ns3="http://services.wisc.edu/l3-lms/space">
        <spaceId>2</spaceId>
        <ns2:user>
                <ns2:pvi>100001</ns2:pvi>
        </ns2:user>
        <role>Instructor</role>
        <active>true</active>
</enrollment>
EOF;

        $observer->expects($this->once())
            ->method('process_enrollment_event')
            ->with($this->isInstanceOf('progress_trace'),
                $this->callback(function(enrollment_event $event) {
                    return $event->spaceId == 2
                            && $event->user->pvi == '100001'
                            && $event->role == role_enum::INSTRUCTOR
                            && $event->active;
                }),
                $this->equalTo('Moodle'),
                $this->anything());

        enrol_lifelonglearning_external::handle_event(array('lmsTarget' => 'Moodle', 'messages' => array($xml)), $observer);

    }


}