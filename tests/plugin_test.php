<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests for lifelonglearning enrolment class.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_lifelonglearning\lmsspace;
use enrol_lifelonglearning\local\caos\schema\enrollment_event;

defined('MOODLE_INTERNAL') || die();

require_once('caos_datasource_mock.php');
require_once('uds_datasource_mock.php');

global $CFG;

/**
 * Tests for lifelonglearning enrolment class.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group      enrol_lifelonglearning
 */
class enrol_lifeltimelearning_plugin_testcase extends advanced_testcase {

    protected function enable_plugin() {
        $enabled = enrol_get_plugins(true);
        $enabled['lifelonglearning'] = true;
        $enabled = array_keys($enabled);
        set_config('enrol_plugins_enabled', implode(',', $enabled));
    }

    protected function disable_plugin() {
        $enabled = enrol_get_plugins(true);
        unset($enabled['lifelonglearning']);
        $enabled = array_keys($enabled);
        set_config('enrol_plugins_enabled', implode(',', $enabled));
    }

    /**
     * @return enrol_lifelonglearning_plugin
     */
    protected function get_plugin() {
        return enrol_get_plugin('lifelonglearning');
    }

    protected function is_enrolled($user, $enrol, $role, $status = ENROL_USER_ACTIVE) {
        global $DB;

        if (!$DB->record_exists('user_enrolments', array('enrolid'=>$enrol->id, 'userid'=>$user->id, 'status'=>$status))) {
            return false;
        }

        return $this->has_role($user, $enrol, $role);
    }

    protected function count_enrollments($enrol, $enrolstatus) {
        global $DB;

        return $DB->count_records('user_enrolments', array('enrolid'=>$enrol->id, 'status'=>$enrolstatus));
    }

    protected function has_role($user, $enrol, $role) {
        global $DB;

        $context = context_course::instance($enrol->courseid);

        if ($role === false) {
            if ($DB->record_exists('role_assignments', array('contextid'=>$context->id, 'userid'=>$user->id, 'component'=>'enrol_lifelonglearning', 'itemid'=>$enrol->id))) {
                return false;
            }
        } else if (!$DB->record_exists('role_assignments', array('contextid'=>$context->id, 'userid'=>$user->id, 'roleid'=>$role->id, 'component'=>'enrol_lifelonglearning', 'itemid'=>$enrol->id))) {
            return false;
        }

        return true;
    }

    /**
     * Test to verify that rosters in moodle update along with the external source.
     *
     * @covers enrol_lifelonglearning_plugin::sync_space_enrollment
     */
    public function test_sync_space1() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        $caos = new enrol_lifelonglearning_caos_datasource_mock();
        $uds = new enrol_lifelonglearning_uds_datasource_mock();

        $this->enable_plugin();
        $plugin = $this->get_plugin();

        $spaces = $caos->getSpaces('Moodle');

        $space1 = $spaces[0];
        $this->assertEquals('Space1', $space1->id);
        $this->assertInstanceOf('enrol_lifelonglearning\local\caos\schema\space', $space1);

        $lmsspace1 = lmsspace::create_space($space1);
        $enrol = $lmsspace1->get_enrol_instance();

        $studentrole = $DB->get_record('role', array('id' => get_config('enrol_lifelonglearning', 'studentrole')));
        $this->assertNotEmpty($studentrole);
        $teacherrole = $DB->get_record('role', array('id' => get_config('enrol_lifelonglearning', 'teacherrole')));
        $this->assertNotEmpty($teacherrole);

        /** Timestep 0 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(0);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace1, $caos, $uds);

        $student1 = $DB->get_record('user', array('idnumber' => 'PVI1'));
        $this->assertNotEmpty($student1);
        $student2 = $DB->get_record('user', array('idnumber' => 'PVI2'));
        $this->assertNotEmpty($student2);
        $teacher1 = $DB->get_record('user', array('idnumber' => 'PVI6'));
        $this->assertNotEmpty($teacher1);

        $this->assertEquals(3, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(0, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole));
        $this->assertTrue($this->is_enrolled($student2, $enrol, $studentrole));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole));

        /** Timestep 1 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(1);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace1, $caos, $uds);

        $this->assertEquals(2, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student2, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));

        /** Timestep 2 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(2);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace1, $caos, $uds);

        $this->assertEquals(2, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student2, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));

        /** Timestep 3 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(3);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace1, $caos, $uds);

        $teacher2 = $DB->get_record('user', array('idnumber' => 'PVI7'));
        $this->assertEmpty($teacher2);
        $this->assertEquals(2, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student2, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));

        /** Timestep 4 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(4);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace1, $caos, $uds);

        $student3 = $DB->get_record('user', array('idnumber' => 'PVI3'));
        $this->assertNotEmpty($student3);
        $this->assertEquals(3, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student2, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student3, $enrol, $studentrole, ENROL_USER_ACTIVE));

        /** Timestep 5 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(5);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace1, $caos, $uds);

        $this->assertEquals(3, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student2, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student3, $enrol, $studentrole, ENROL_USER_ACTIVE));

        /** Config change:  Set teacherroleid to studentroleid. */
        $plugin->set_config('teacherrole', get_config('enrol_lifelonglearning', 'studentrole'));
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace1, $caos, $uds);

        $this->assertEquals(3, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student2, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertFalse($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student3, $enrol, $studentrole, ENROL_USER_ACTIVE));
    }

    /**
     * Simple sync test with only one teacher enrollment.
     */
    public function test_sync_space2() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        $caos = new enrol_lifelonglearning_caos_datasource_mock();
        $uds = new enrol_lifelonglearning_uds_datasource_mock();

        $this->enable_plugin();
        $plugin = $this->get_plugin();

        $spaces = $caos->getSpaces('Moodle');

        $space2 = $spaces[1];
        $this->assertInstanceOf('enrol_lifelonglearning\local\caos\schema\space', $space2);
        $this->assertEquals('Space2', $space2->id);

        $lmsspace2 = lmsspace::create_space($space2);
        $enrol = $lmsspace2->get_enrol_instance();
        $course = $DB->get_record('course', array('id'=>$enrol->courseid));
        $this->assertNotEmpty($course);

        $teacherrole = $DB->get_record('role', array('id' => get_config('enrol_lifelonglearning', 'teacherrole')));
        $this->assertNotEmpty($teacherrole);

        /** Timestep 0 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(0);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace2, $caos, $uds);

        $teacher1 = $DB->get_record('user', array('idnumber' => 'PVI6'));
        $this->assertNotEmpty($teacher1);

        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(0, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole));
    }

    /**
     * This is a long test which verifies ferpa student role behavior, as well as behavior when changing
     * role settings in the plugin.
     */
    public function test_sync_space3() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        $caos = new enrol_lifelonglearning_caos_datasource_mock();
        $uds = new enrol_lifelonglearning_uds_datasource_mock();
        $wisc = new local_wiscservices_plugin(new null_progress_trace());
        $wisc->set_datasource($uds);

        $this->enable_plugin();
        $plugin = $this->get_plugin();

        $spaces = $caos->getSpaces('Moodle');

        $space3 = $spaces[2];
        $this->assertEquals('Space3', $space3->id);
        $this->assertInstanceOf('enrol_lifelonglearning\local\caos\schema\space', $space3);

        $lmsspace3 = lmsspace::create_space($space3);
        $enrol = $lmsspace3->get_enrol_instance();

        // Different ferpa and student roles
        $ferpastudentrole = $DB->get_record('role', array('shortname'=>'teacher'));
        $studentrole = $DB->get_record('role', array('shortname'=>'student'));
        $teacherrole = $DB->get_record('role', array('shortname'=>'editingteacher'));

        $this->assertNotEmpty($ferpastudentrole);
        $this->assertNotEmpty($studentrole);
        $this->assertNotEmpty($teacherrole);

        set_config('ferpastudentrole', $ferpastudentrole->id, 'enrol_lifelonglearning');
        set_config('studentrole', $studentrole->id, 'enrol_lifelonglearning');
        set_config('teacherrole', $teacherrole->id, 'enrol_lifelonglearning');

        // Make student 4 and 5 ferpa students
        $udsstudent4 = $uds->get_mock_user('PVI4');
        $udsstudent4->ferpaName = true;
        $udsstudent5 = $uds->get_mock_user('PVI5');
        $udsstudent5->ferpaName = true;

        /** Timestep 0 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(0);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $student1 = $DB->get_record('user', array('idnumber' => 'PVI1'));
        $this->assertNotEmpty($student1);
        $student2 = $DB->get_record('user', array('idnumber' => 'PVI2'));
        $this->assertNotEmpty($student2);
        $student3 = $DB->get_record('user', array('idnumber' => 'PVI3'));
        $this->assertNotEmpty($student3);
        $student4 = $DB->get_record('user', array('idnumber' => 'PVI4'));
        $this->assertNotEmpty($student4);
        $student5 = $DB->get_record('user', array('idnumber' => 'PVI5'));
        $this->assertNotEmpty($student5);
        $teacher1 = $DB->get_record('user', array('idnumber' => 'PVI6'));
        $this->assertNotEmpty($teacher1);

        // Check that ferpa flags are being set
        profile_load_custom_fields($student4);
        $this->assertEquals("1", $student4->profile['ferpaName']);
        profile_load_custom_fields($student5);
        $this->assertEquals("1", $student5->profile['ferpaName']);

        $this->assertEquals(6, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(0, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole));
        $this->assertTrue($this->is_enrolled($student2, $enrol, $studentrole));
        $this->assertTrue($this->is_enrolled($student3, $enrol, $studentrole));
        $this->assertTrue($this->is_enrolled($student4, $enrol, $ferpastudentrole));
        $this->assertTrue($this->is_enrolled($student5, $enrol, $ferpastudentrole));
        $this->assertFalse($this->is_enrolled($student1, $enrol, $ferpastudentrole));
        $this->assertFalse($this->is_enrolled($student4, $enrol, $studentrole));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole));

        /** Timestep 1 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(1);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(3, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(3, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student2, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student3, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student4, $enrol, $ferpastudentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student5, $enrol, $ferpastudentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));

        /** Timestep 2 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(2);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(5, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student2, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student3, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student4, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student5, $enrol, $ferpastudentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));

        // Switch settings so that ferpa role == student role
        set_config('ferpastudentrole', $studentrole->id, 'enrol_lifelonglearning');
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(5, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student2, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student3, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertTrue($this->is_enrolled($student4, $enrol, false, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student5, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertFalse($this->is_enrolled($student5, $enrol, $ferpastudentrole, ENROL_USER_ACTIVE));
        $this->assertFalse($this->is_enrolled($student5, $enrol, $ferpastudentrole, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($teacher1, $enrol, $teacherrole, ENROL_USER_ACTIVE));

        // Switch settings so that ferpa students are not enrolled
        set_config('ferpastudentrole', 0, 'enrol_lifelonglearning');
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(4, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(2, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));

        // Switch settings so that all students are not enrolled
        set_config('studentrole', 0, 'enrol_lifelonglearning');
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(5, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));

        // Switch settings back to separate roles
        set_config('ferpastudentrole', $ferpastudentrole->id, 'enrol_lifelonglearning');
        set_config('studentrole', $studentrole->id, 'enrol_lifelonglearning');
        set_config('teacherrole', $teacherrole->id, 'enrol_lifelonglearning');

        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(5, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));

        // Make student 1 a ferpa student
        $udsstudent1 = $uds->get_mock_user('PVI1');
        $udsstudent1->ferpaName = true;
        $this->assertNotEmpty($wisc->verify_person($udsstudent1, true));

        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(5, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $ferpastudentrole, ENROL_USER_ACTIVE));
        $this->assertFalse($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));

        // Make student 5 a non-ferpa student
        $udsstudent5->ferpaName = false;
        $this->assertNotEmpty($wisc->verify_person($udsstudent5, true));

        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(5, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student5, $enrol, $studentrole, ENROL_USER_ACTIVE));
        $this->assertFalse($this->is_enrolled($student5, $enrol, $ferpastudentrole, ENROL_USER_ACTIVE));

        // Remove all roles from config.
        set_config('ferpastudentrole', 0, 'enrol_lifelonglearning');
        set_config('studentrole', 0, 'enrol_lifelonglearning');
        set_config('teacherrole', 0, 'enrol_lifelonglearning');
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace3, $caos, $uds);

        $this->assertEquals(0, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(6, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
    }

    /**
     * Test enrollment event handler.
     */
    public function test_process_enrollment_event() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        $uds = new enrol_lifelonglearning_uds_datasource_mock();
        $caos = new enrol_lifelonglearning_caos_datasource_mock();

        $this->enable_plugin();
        $plugin = $this->get_plugin();

        set_config('lmstarget', 'SomethingElse,Moodle', 'enrol_lifelonglearning');

        $studentrole = $DB->get_record('role', array('shortname'=>'student'));
        $teacherrole = $DB->get_record('role', array('shortname'=>'editingteacher'));

        $spaces = $caos->getSpaces('Moodle');

        $space2 = $spaces[1];
        $this->assertInstanceOf('enrol_lifelonglearning\local\caos\schema\space', $space2);
        $this->assertEquals('Space2', $space2->id);

        $lmsspace2 = lmsspace::create_space($space2);
        $enrol = $lmsspace2->get_enrol_instance();
        $course = $DB->get_record('course', array('id'=>$enrol->courseid));
        $this->assertNotEmpty($course);

        $teacherrole = $DB->get_record('role', array('id' => get_config('enrol_lifelonglearning', 'teacherrole')));
        $this->assertNotEmpty($teacherrole);

        /** Timestep 0 -- @see enrol_lifelonglearning_caos_datasource_mock::getRosters */
        $caos->set_timestep(0);
        $plugin->sync_space_enrollment(new null_progress_trace(), $lmsspace2, $caos, $uds);

        // Enrol PVI1.
        $event = new enrollment_event(
                array(
                    'spaceId' => $space2->id,
                    'role' => 'Learner',
                    'active' => true,
                    'user' =>  array('pvi' => 'PVI1'),
                 )
        );
        $this->assertTrue($plugin->process_enrollment_event(new null_progress_trace(), $event, 'Moodle', $uds));
        $this->assertEquals(2, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $student1 = $DB->get_record('user', array('idnumber' => 'PVI1'));
        $this->assertNotEmpty($student1);
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));

        // Unenrol PVI1.
        $event = new enrollment_event(
                array(
                    'spaceId' => $space2->id,
                    'role' => 'Learner',
                    'active' => false,
                    'user' =>  array('pvi' => 'PVI1'),
                )
        );
        $this->assertTrue($plugin->process_enrollment_event(new null_progress_trace(), $event, 'Moodle', $uds));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(1, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, false, ENROL_USER_SUSPENDED));

        // Enrol PVI1.
        $event = new enrollment_event(
                array(
                    'spaceId' => $space2->id,
                    'role' => 'Learner',
                    'active' => true,
                    'user' =>  array('pvi' => 'PVI1'),
                )
        );
        $this->assertTrue($plugin->process_enrollment_event(new null_progress_trace(), $event, 'Moodle', $uds));
        $this->assertEquals(2, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(0, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));

        // Same event again
        $this->assertTrue($plugin->process_enrollment_event(new null_progress_trace(), $event, 'Moodle', $uds));
        $this->assertEquals(2, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(0, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $this->assertTrue($this->is_enrolled($student1, $enrol, $studentrole, ENROL_USER_ACTIVE));

        // Unenrol PVI2 (not enrolled).
        $event = new enrollment_event(
                array(
                    'spaceId' => $space2->id,
                    'role' => 'Learner',
                    'active' => false,
                    'user' =>  array('pvi' => 'PVI2'),
                )
        );
        $this->assertTrue($plugin->process_enrollment_event(new null_progress_trace(), $event, 'Moodle', $uds));
        // No change to enrollment
        $this->assertEquals(2, $this->count_enrollments($enrol, ENROL_USER_ACTIVE));
        $this->assertEquals(0, $this->count_enrollments($enrol, ENROL_USER_SUSPENDED));
        $student2 = $DB->get_record('user', array('idnumber' => 'PVI2'));
        $this->assertFalse($this->is_enrolled($student2, $enrol, false, ENROL_USER_SUSPENDED));
    }

    /**
     * Test enrollment event handler with wrong lmsTarget.
     */
    public function test_process_enrollment_event_wrong_target() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        $uds = new enrol_lifelonglearning_uds_datasource_mock();
        $caos = new enrol_lifelonglearning_caos_datasource_mock();

        $this->enable_plugin();
        $plugin = $this->get_plugin();

        $plugin->sync_all_spaces(new null_progress_trace(), 'Moodle', $caos, $uds);

        set_config('lmstarget', 'SomethingElse,Moodle', 'enrol_lifelonglearning');

        // Enrol PVI1 with wrong lmsTarget.
        $event = new enrollment_event(
                array(
                        'spaceId' => 'Space1',
                        'role' => 'Learner',
                        'active' => false,
                        'user' =>  array('pvi' => 'PVI1'),
                )
        );
        $this->setExpectedException('moodle_exception');
        $this->assertFalse($plugin->process_enrollment_event(new null_progress_trace(), $event, 'DifferentMoodle', $uds));
    }

    /**
     * Test enrollment event handler with missing space.
     */
    public function test_process_enrollment_event_no_space() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        $uds = new enrol_lifelonglearning_uds_datasource_mock();
        $caos = new enrol_lifelonglearning_caos_datasource_mock();

        $this->enable_plugin();
        $plugin = $this->get_plugin();

        $plugin->sync_all_spaces(new null_progress_trace(), 'Moodle', $caos, $uds);

        set_config('lmstarget', 'SomethingElse,Moodle', 'enrol_lifelonglearning');

        // Enrol PVI1 with wrong lmsTarget.
        $event = new enrollment_event(
                array(
                        'spaceId' => 'NoSuchSpace',
                        'role' => 'Learner',
                        'active' => true,
                        'user' =>  array('pvi' => 'PVI1'),
                )
        );
        $this->setExpectedException('moodle_exception');
        $this->assertFalse($plugin->process_enrollment_event(new null_progress_trace(), $event, 'Moodle', $uds));
    }

    /**
     * Test enrollment event handler with missing netid in datasource.
     */
    public function test_process_enrollment_event_patially_setup_user() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        $uds = new enrol_lifelonglearning_uds_datasource_mock();
        $caos = new enrol_lifelonglearning_caos_datasource_mock();

        $this->enable_plugin();
        $plugin = $this->get_plugin();

        $spaces = $caos->getSpaces('Moodle');

        $space1 = $spaces[0];
        $this->assertEquals('Space1', $space1->id);
        $this->assertInstanceOf('enrol_lifelonglearning\local\caos\schema\space', $space1);
        $lmsspace1 = lmsspace::create_space($space1);
        $enrol = $lmsspace1->get_enrol_instance();
        $studentrole = $DB->get_record('role', array('shortname'=>'student'));

        set_config('lmstarget', 'Moodle', 'enrol_lifelonglearning');

        // PVI1 has no netid
        $mockuser = $uds->get_mock_user('PVI1');
        $mockuser->netid = null;

        // Enrol PVI1
        $event = new enrollment_event(
                array(
                        'spaceId' => 'Space1',
                        'role' => 'Learner',
                        'active' => true,
                        'user' =>  array('pvi' => 'PVI1'),
                )
        );

        $this->assertFalse($plugin->process_enrollment_event(new null_progress_trace(), $event, 'Moodle', $uds));

        // Ensure user was created with 'missingnetid' username
        $user = $DB->get_record('user', array('idnumber' => 'PVI1'), '*', MUST_EXIST);
        $this->assertNotEmpty($user);

        $this->assertEquals('missingnetid.pvi1@wisc.edu', $user->username);
        $this->assertTrue($this->is_enrolled($user, $enrol, $studentrole, ENROL_USER_ACTIVE));

        // PVI1 gets a netid.
        $mockuser->netid = 'astudent';

        // Run a full sync
        $plugin->sync_all_spaces(new null_progress_trace(), 'Moodle', $caos, $uds);

        $user = $DB->get_record('user', array('idnumber' => 'PVI1'), '*', MUST_EXIST);
        $this->assertNotEmpty($user);

        // Ensure the username was updates
        $this->assertEquals('astudent@wisc.edu', $user->username);
        $this->assertTrue($this->is_enrolled($user, $enrol, $studentrole, ENROL_USER_ACTIVE));

        // Clear the PVI in moodle again
        $DB->set_field('user', 'username', 'missingnetid.pvi1@wisc.edu', array('id'=>$user->id));

        // Run the login hook to simulate a login.  Here we pass the EPPN.
        local_wiscservices_plugin::shibboleth_login_hook('astudent@wisc.edu', $mockuser->pvi, $uds);

        $user = $DB->get_record('user', array('idnumber' => 'PVI1'), '*', MUST_EXIST);
        $this->assertNotEmpty($user);

        // Ensure the username was set properly
        $this->assertEquals('astudent@wisc.edu', $user->username);
        $this->assertTrue($this->is_enrolled($user, $enrol, $studentrole, ENROL_USER_ACTIVE));
    }

    /**
     * Verify that a full sync creates all the upstream spaces.
     */
    public function test_sync_all_spaces() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        $caos = new enrol_lifelonglearning_caos_datasource_mock();
        $uds = new enrol_lifelonglearning_uds_datasource_mock();

        $this->enable_plugin();
        $plugin = $this->get_plugin();

        $plugin->sync_all_spaces(new null_progress_trace(), 'Moodle', $caos, $uds);

        $this->assertEquals(1, $DB->count_records('course', array('fullname'=>'Test course 1')));
        $this->assertEquals(1, $DB->count_records('course', array('fullname'=>'Test course 2')));
        $this->assertEquals(1, $DB->count_records('course', array('fullname'=>'Ferpa test course 1')));
    }


}