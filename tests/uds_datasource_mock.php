<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mock UDS datasource for lifelonglearning enrolment tests.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use local_wiscservices\local\uds\datasource;
use local_wiscservices\local\uds\uds_person;

defined('MOODLE_INTERNAL') || die();

global $CFG;

class enrol_lifelonglearning_uds_datasource_mock implements datasource {

    protected $devmode = 0;
    protected $timeout = 10;

    protected $people;

    public function __construct() {
        $this->people['PVI1'] = self::create_uds_person('Test', 'Student 1', 'user1@doesntexist.com', 'user1', 'PVI1');
        $this->people['PVI2'] = self::create_uds_person('Test', 'Student 2', 'user2@doesntexist.com', 'user2', 'PVI2');
        $this->people['PVI3'] = self::create_uds_person('Test', 'Student 3', 'user3@doesntexist.com', 'user3', 'PVI3');
        $this->people['PVI4'] = self::create_uds_person('Test', 'Student 4', 'user4@doesntexist.com', 'user4', 'PVI4');
        $this->people['PVI5'] = self::create_uds_person('Test', 'Student 5', 'user5@doesntexist.com', 'user5', 'PVI5');
        $this->people['PVI6'] = self::create_uds_person('Test', 'Teacher 1', 'user6@doesntexist.com', 'user6', 'PVI6');
        $this->people['PVI7'] = self::create_uds_person('Test', 'Teacher 2', 'user7@doesntexist.com', 'user7', 'PVI7');
    }

    /**
     * Update or create a mock user.
     * @param uds_person $person
     */
    public function set_mock_user(uds_person $person) {
        $this->people[$person->pvi] = $person;
    }

    /**
     * Get a mock user by pvi.
     * @param string $pvi
     * @return uds_person
     */
    public function get_mock_user($pvi) {
        return $this->people[$pvi];
    }

    /* (non-PHPdoc)
     * @see \local_wiscservices\local\uds\datasource::build_people_query()
     */
    public function build_people_query($source, $idname, $values) {
        if (!is_array($values)) {
            $values = array($values);
        }
        $queries = array();
        foreach ($values as $value) {
            $queries[] = array('Source'=>$source, 'IdName'=>$idname, 'Value'=>$value);
        }
        return $queries;
    }

    /* (non-PHPdoc)
     * @see \local_wiscservices\local\uds\datasource::get_people()
     */
    public function get_people(array $queries) {
        $result = array();
        foreach ($queries as $query) {
            $type = $query['Source'].'-'.$query['IdName'];
            switch($type) {
                // Only PVI queries handled
                case 'UWMSNSUDS-pvi':
                    if (isset($this->people[$query['Value']])) {
                        $result[] = $this->people[$query['Value']];
                    } else {
                        // not found, so ignore
                    }
                    break;

                default:
                    new coding_exception('Invalid query: '.$type);
            }
        }
        return $result;
    }

    /* (non-PHPdoc)
     * @see \local_wiscservices\local\uds\datasource::get_devmode()
     */
    public function get_devmode() {
        return $this->devmode;
    }

    /* (non-PHPdoc)
     * @see \local_wiscservices\local\uds\datasource::get_maxquery()
     */
    public function get_maxquery() {
        return 5;
    }

    /* (non-PHPdoc)
     * @see \local_wiscservices\local\uds\datasource::set_devmode()
     */
    public function set_devmode($dev) {
        $this->devmode = $dev;
    }

    /* (non-PHPdoc)
     * @see \local_wiscservices\local\soap\datasource::get_timeout()
     */
    public function get_timeout() {
        return $this->timeout;
    }

    /* (non-PHPdoc)
     * @see \local_wiscservices\local\soap\datasource::set_timeout()
     */
    public function set_timeout($timeout) {
        $this->timeout = $timeout;
    }

    public static function create_uds_person($firstname, $lastname, $email, $netid, $pvi) {
        $o = new uds_person();
        $o->firstName = $firstname;
        $o->lastName = $lastname;
        $o->email = $email;
        $o->netid = $netid;
        $o->pvi = $pvi;
        $o->roles = array('testaccount');
        return $o;
    }
    /**
     * {@inheritDoc}
     * @see \local_wiscservices\local\uds\datasource::get_pvi_change_history()
     */
    public function get_pvi_change_history($pvi) {
        return array($pvi);
    }


}
