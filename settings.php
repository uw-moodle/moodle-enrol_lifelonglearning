<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lifelong learning enrolment plugin system settings.
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    // --- heading ---
    $settings->add(new admin_setting_heading('enrol_lifelonglearning_settings', '', get_string('pluginname_desc', 'enrol_lifelonglearning')));

    // --- connection settings ---
    $settings->add(new admin_setting_heading('enrol_lifelonglearning_server_settings', get_string('serversettings', 'enrol_lifelonglearning'), ''));
    $settings->add(new admin_setting_configtext('enrol_lifelonglearning/soapurl', get_string('soapurl_key', 'enrol_lifelonglearning'), '', 'http://esb.services.wisc.edu/esbv2/CAOS/WebService/l3-ws-1.0/l3.wsdl', PARAM_URL,60));
    $settings->add(new admin_setting_configtext('enrol_lifelonglearning/soapuser', get_string('soapuser_key', 'enrol_lifelonglearning'), '', '', PARAM_RAW));
    $settings->add(new admin_setting_configpasswordunmask('enrol_lifelonglearning/soappass', get_string('soappass_key', 'enrol_lifelonglearning'), '', '', PARAM_RAW));
    $settings->add(new admin_setting_configtext('enrol_lifelonglearning/lmstarget', get_string('lmstarget_key', 'enrol_lifelonglearning'), get_string('lmstarget', 'enrol_lifelonglearning'), '', PARAM_RAW));

    // --- role mapping settings ---
    $settings->add(new admin_setting_heading('enrol_roles', get_string('rolemapping', 'enrol_lifelonglearning'), ''));
    if (!during_initial_install()) {
        $options = get_default_enrol_roles(context_system::instance());
        $options[0] = get_string('dontsync', 'enrol_lifelonglearning');
        $student = get_archetype_roles('student');
        $student = reset($student);
        $settings->add(new admin_setting_configselect('enrol_lifelonglearning/studentrole',
                get_string('studentrole_key', 'enrol_lifelonglearning'), '', $student->id, $options));
        $settings->add(new admin_setting_configselect('enrol_lifelonglearning/ferpastudentrole',
                get_string('ferpastudentrole_key', 'enrol_lifelonglearning'), '', $student->id, $options));
        $editingteacher = get_archetype_roles('editingteacher');
        $editingteacher = reset($editingteacher);
        $settings->add(new admin_setting_configselect('enrol_lifelonglearning/teacherrole',
                get_string('teacherrole_key', 'enrol_lifelonglearning'), '', $editingteacher->id, $options));
    }

    // --- course creation settings ---
    $settings->add(new admin_setting_heading('enrol_lifelonglearning_coursecreation', get_string('coursecreation', 'enrol_lifelonglearning'), ''));
    $displaylist = array(0 => get_string('none'));
    $displaylist += coursecat::make_categories_list();
    $settings->add(new admin_setting_configselect('enrol_wisc/category', get_string('category_key', 'enrol_wisc'), get_string('category', 'enrol_wisc'),
            0, $displaylist));
    $settings->add(new admin_setting_configtext('enrol_lifelonglearning/template', get_string('template_key', 'enrol_lifelonglearning'), get_string('template', 'enrol_lifelonglearning'), ''));
}
