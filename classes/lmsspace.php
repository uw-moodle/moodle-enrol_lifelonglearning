<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * LMS Space class for lifelong learning enrolment plugin
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning;

use local_wiscservices\local\uds\datasource as uds_datasource;
use enrol_lifelonglearning\local\caos\schema\space;

require_once($CFG->libdir.'/coursecatlib.php');

defined('MOODLE_INTERNAL') || die;

/**
 * LMS Space class for lifelong learning enrolment plugin
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class lmsspace {

    /** @var stdClass the db record */
    protected $rec;

    // Enrol record custom field names.

    /** @var string spaceid field in enrol record */
    const SPACEID_FIELD = 'customchar1';

    /** @var string lmstarget field in enrol record */
    const LMSTARGET_FIELD = 'customchar2';

    /** @var string title field in enrol record */
    const TITLE_FIELD = 'customtext1';

    /** @var string category / subcategory field in enrol record */
    const CATEGORY_FIELD = 'customtext2';

    /** @var int enddate field in enrol record */
    const ENDDATE_FIELD = 'customint1';

    /** @var int last full sync time */
    const SYNCTIME_FIELD = 'customint2';

    /** @var int last roster update time */
    const UPDATETIME_FIELD = 'customint3';



    /**
     * Private constructor.
     *
     * This class can be instantiated with the load_* and create_* static functions.
     *
     * @param stdClass $rec the enrol record
     * @param datasource $caos
     * @param uds_datasource $uds
     */
    private function __construct(\stdClass $rec) {
        $this->rec = $rec;
    }

    /**
     * Load lmsspace from an existing enrol instance record.
     *
     * @param stdClass $instance
     * @return lmsspace|null
     */
    public static function load_space_by_instance(\stdClass $instance) {

        if ($instance->enrol != 'lifelonglearning') {
            throw new \coding_exception('invalid instance type');
        }
        return new static($instance);
    }

    /**
     * Load lmsspace from db, by spaceid and lmstarget.
     *
     * @param string $spaceid
     * @param string $lmstarget
     * @param datasource $caos
     * @param uds_datasource $uds
     * @return lmsspace|null
     */
    public static function load_space_by_id($spaceid, $lmstarget) {

        global $DB;

        $cond = array('enrol' => 'lifelonglearning',
            self::SPACEID_FIELD => $spaceid,
            self::LMSTARGET_FIELD => $lmstarget);

        $rec = $DB->get_record('enrol', $cond);

        if (!$rec) {
            return null;
        }

        return new static($rec);
    }

    /**
     * Load lmsspace from db, by space.
     *
     * @param space $space
     * @return lmsspace|null
     */
    public static function load_space(space $space) {
        return static::load_space_by_id($space->id, $space->lmsTarget);
    }

    /**
     * Create a lmsspace object from @see{space}.
     *
     * If the space already exists, then load and update the existing space.
     *
     * @param space $space
     * @param stdClass $course the moodle course, if adding to an existing course.
     * @return lmsspace
     *
     * @throws \moodle_exception if the space already exists.
     */
    public static function create_space(space $space, \stdClass $course = null) {

        global $DB;

        // See if the space already exists.
        if ($lmsspace = static::load_space($space)) {
            $lmsspace->update($space);
            return $lmsspace;
        }

        $enrol = enrol_get_plugin('lifelonglearning');

        $transaction = $DB->start_delegated_transaction();

        if (!$course) {
            $course = static::create_moodle_course($space);
        }
        $enrol->add_instance($course, static::make_custom_fields($space));

        $DB->commit_delegated_transaction($transaction);

        return static::load_space($space);
    }

    /**
     * Update the stored lmsspace to match the provided space record.
     *
     * @param space $space
     */
    public function update(space $space) {
        global $DB;
        if ($space->id != $this->get_spaceid() ||
                $space->lmsTarget != $this->get_lmstarget()) {
            // Not allowed to change space identifiers with an update.
            throw new \moodle_exception('spaceupdatefailed');
        }
        $fields = static::make_custom_fields($space);
        $update = false;
        foreach ($fields as $key => $value) {
            if ($this->rec->{$key} != $value) {
                $update = true;
                break;
            }
        }
        if ($update) {
            $fields['id'] = $this->rec->id;
            $DB->update_record('enrol', $fields);
            // reload record
            $this->rec = $DB->get_record('enrol', array('id' => $this->rec->id), '*', MUST_EXIST);
        }
    }

    /**
     * @return string lmstarget
     */
    public function get_lmstarget() {
        return $this->rec->{self::LMSTARGET_FIELD};
    }

    /**
     * @return string spaceid
     */
    public function get_spaceid() {
        return $this->rec->{self::SPACEID_FIELD};
    }

    /**
     * @return string spaceid
     */
    public function get_title() {
        return $this->rec->{self::TITLE_FIELD};
    }

    /**
     * @return string categories
     */
    public function get_categories() {
        return $this->rec->{self::CATEGORY_FIELD};
    }

    /**
     * @return int timestamp of last full sync
     */
    public function get_time_fullsync() {
        return $this->rec->{self::SYNCTIME_FIELD};
    }

    /**
     * @return int timestamp of last roster update sync
     */
    public function get_time_rosterupdate() {
        return $this->rec->{self::UPDATETIME_FIELD};
    }

    /**
     * Set full sync time in DB.
     *
     * @param int $time
     */
    public function set_time_fullsync($time) {
        global $DB;
        $this->rec->{self::SYNCTIME_FIELD} = $time;
        $DB->set_field('enrol', self::SYNCTIME_FIELD, $time, array('id' => $this->rec->id));
    }

    /**
     * Set roster update time in DB.
     *
     * @param int $time
     */
    public function set_time_rosterupdate($time) {
        global $DB;
        $this->rec->{self::UPDATETIME_FIELD} = $time;
        $DB->set_field('enrol', self::UPDATETIME_FIELD, $time, array('id' => $this->rec->id));
    }


    /**
     * Get the enrol record
     *
     * @return stdClass
     */
    public function get_enrol_instance() {
        return $this->rec;
    }

    /**
     * Create a new moodle course for the given space.
     *
     * This creates an empty course with no enrol_lifelonglearning instance.
     *
     * @param space $space
     * @return stdClass the moodle course
     */
    protected static function create_moodle_course(space $space) {
        global $CFG, $DB;

        require_once("$CFG->dirroot/course/lib.php");

        // Override defaults with template course
        $template = false;
        if (get_config('enrol_lifelonglearning', 'template')) {
            if ($template = $DB->get_record('course', array('shortname'=>get_config('enrol_lifelonglearning', 'template')))) {
                $template = fullclone(course_get_format($template)->get_course());
                unset($template->id); // So we are clear to reinsert the record
                unset($template->fullname);
                unset($template->shortname);
                unset($template->idnumber);
            }
        }
        if (!$template) {
            $courseconfig = get_config('moodlecourse');
            $template = new \stdClass();
            $template->summary        = '';
            $template->summaryformat  = FORMAT_HTML;
            $template->format         = $courseconfig->format;
            $template->newsitems      = $courseconfig->newsitems;
            $template->showgrades     = $courseconfig->showgrades;
            $template->showreports    = $courseconfig->showreports;
            $template->maxbytes       = $courseconfig->maxbytes;
            $template->groupmode      = $courseconfig->groupmode;
            $template->groupmodeforce = $courseconfig->groupmodeforce;
            $template->visible        = $courseconfig->visible;
            $template->lang           = $courseconfig->lang;
            $template->enablecompletion = $courseconfig->enablecompletion;
        }
        $course = $template;

        // Override with data from space.
        $course->category  = static::get_moodle_category($space);
        $course->fullname  = $space->spaceProvisioning->title;
        if ($space->spaceProvisioning->shortTitle) {
            $course->shortname = $space->spaceProvisioning->shortTitle;
        } else {
            $course->shortname = static::generate_default_shortname($space);
        }

        // Compute course dates.
        if ($space->spaceProvisioning->startDate) {
            $startdate = strtotime($space->spaceProvisioning->startDate);
        } else {
            $startdate = time();
        }

        // Pretend that the course starts on a Sunday (even though it probably doesn't) so that our
        // weeks start on a Sunday.
        $dateinfo = getdate( $startdate );
        $coursestart = $startdate - $dateinfo['wday'] * 60 * 60 * 24;
        $course->startdate = $coursestart;


        if ($space->spaceProvisioning->endDate) {
            $enddate = strtotime($space->spaceProvisioning->endDate);
            $timespan = $enddate - $coursestart;
            $hours = $timespan / 3600;
            $days = $hours / 24;
            $weeks = ceil( $days / 7 );
            $course->numsections = $weeks;
        }

        if (empty($course->fullname) || empty($course->shortname)) {
            // We are in trouble!
            throw new \moodle_exception('cannotcreatecourse', 'enrol_lifelonglearning');
        }

        // Check if the shortname already exists , and if it does change it.
        if ($DB->record_exists('course', array('shortname' => $course->shortname))) {
            $suffix = 0;
            while ($DB->record_exists('course', array('shortname' => $course->shortname.'-'.$suffix))) {
                $suffix++;
            }
            $course->shortname = $course->shortname.'-'.$suffix;
        }

        $newcourse = create_course($course);
        return $newcourse;
    }

    /**
     * Convert a category id to the corresponding moodle categoryid.
     *
     * We modify the coas category id to minimize conflicts with other categoryid's in the system.
     *
     * @param string $categoryid
     * @return string
     */
    public static function generate_categoryid($categoryid) {
        return 'LLL-'.$categoryid;
    }

    /**
     * Generate the default shortname for a course.  This may be changed during course creation
     * if there is a conflict with an existing course.
     *
     * @param space $space
     * @return string
     */
    public static function generate_default_shortname(space $space) {
        return 'LLL-'.$space->id;
    }

    /**
     * Convert a moodle categoryid to a spaceid.
     *
     * @param string $categoryid
     * @return string|null  spaceid or null
     */
    public static function categoryid_to_spaceid($categoryid) {
        $matches = array();
        if (preg_match('/^LLL-(.*)$/', $categoryid, $matches)) {
            // LLL category
            $spaceid = $matches[1];
        } else {
            // not LLL category
            $spaceid = null;
        }
        return $spaceid;
    }

    /**
     * Get the moodle category id for a new lms space, creating if necessary.
     *
     * We use the convention of prefixing the CAOS category id's by 'LLL-' to
     * obtain the moodle category idnumber.  The category name is initially set to
     * the idnumber, but can be changed in the moodle interface.
     *
     * @param space $space
     * @return int the category id
     *
     * @throws \moodle_exception
     */
    protected static function get_moodle_category(space $space) {
        global $DB, $CFG;

        $subcategory = $space->subcategory;
        $category = $space->category;

        if (!$category) {
            // No category (or only a subcategory) so use the default.
            return $CFG->defaultrequestcategory;
        }

        // See if the main category exists.
        $parent =  $DB->get_field('course_categories', 'id', array('idnumber' => static::generate_categoryid($category->id)));
        if ($parent === false) {
            // Create main category
            $data = new \stdClass();
            $data->name = $category->description;
            $data->idnumber = static::generate_categoryid($category->id);
            $data->parent = get_config('enrol_lifelonglearning', 'category');
            $data->description = get_string('defaultcategorydesc', 'enrol_lifelonglearning');
            $data->descriptionformat = FORMAT_HTML;
            $parent = \coursecat::create($data)->id;
        }

        if (!$subcategory) {
            return $parent;
        }

        // Now see if the sub category exists.
        $child =  $DB->get_field('course_categories', 'id', array('idnumber' => static::generate_categoryid($category->id.'/'.$subcategory->id), 'parent' => $parent));
        if ($child === false) {
            // Create the sub category
            $data = new \stdClass();
            $data->name = $subcategory->description;
            $data->idnumber = static::generate_categoryid($category->id.'/'.$subcategory->id);
            $data->parent = $parent;
            $data->description = get_string('defaultcategorydesc', 'enrol_lifelonglearning');
            $data->descriptionformat = FORMAT_HTML;
            $child = \coursecat::create($data)->id;
        }
        return $child;
    }

    /**
     * Generate enrol record custom fields from a space record.
     *
     * @param space $space
     * @return array
     */
    protected static function make_custom_fields(space $space) {
        if ($space->category) {
            $categorydesc = $space->category->description;
            if ($space->subcategory) {
                $categorydesc .= '/'.$space->subcategory->description;
            }
        } else {
            $categorydesc = '';
        }
        return array(
            self::SPACEID_FIELD => $space->id,
            self::LMSTARGET_FIELD => $space->lmsTarget,
            self::CATEGORY_FIELD => $categorydesc,
            self::TITLE_FIELD => $space->spaceProvisioning->title,
            self::ENDDATE_FIELD => strtotime($space->spaceProvisioning->endDate),
        );
    }
}