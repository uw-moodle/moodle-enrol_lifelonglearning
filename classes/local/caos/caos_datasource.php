<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Datastore CAOS implementation for lifelong learning enrolment plugin
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos;

use enrol_lifelonglearning\local\caos\schema\roster;
use enrol_lifelonglearning\local\caos\schema\space;



defined('MOODLE_INTERNAL') || die;

/**
 * Datastore CAOS implementation for lifelong learning enrolment plugin
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class caos_datasource implements datasource {

    protected function get_soapclient() {
        return soapclient::get();
    }

    protected static function object_to_array($obj) {
        if(is_object($obj)) {
            $obj = (array) $obj;
        }
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = self::object_to_array($val);
            }
            $obj = $new;
        }
        return $obj;
    }

    /**
     * (non-PHPdoc)
     * @see \enrol_lifelonglearning\local\caos\datasource::getRosters()
     */
    public function getRosters($lmstarget, array $spaceids = array()) {
        if (!empty($spaceids)) {
            $params = array('spaceIds'=>$spaceids);
        } else {
            $params = array('lmsTarget'=>$lmstarget);
        }
        $data = $this->get_soapclient()->GetRosters($params);
        $result = array();
        if (!empty($data->rosters->roster)) {
            foreach ($data->rosters->roster as $roster) {
                // Fix the enrollments element.  CAOS uses an anonymous complexType to contain the enrollments.
                if (!empty($roster->enrollments->enrollment)) {
                    $roster->enrollments = $roster->enrollments->enrollment;
                } else {
                    $roster->enrollments = array();
                }
                $result[] = new roster(self::object_to_array($roster));
            }
        }
        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see \enrol_lifelonglearning\local\caos\datasource::getSpaces()
     */
    public function getSpaces($lmstarget) {
        $params = array('lmsTarget'=>$lmstarget);
        $data = $this->get_soapclient()->GetSpaces($params);
        $result = array();
        if (!empty($data->spaces->space)) {
            foreach ($data->spaces->space as $space) {
                $result[] = new space(self::object_to_array($space));
            }
        }
        return $result;
    }
}