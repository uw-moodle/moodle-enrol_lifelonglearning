<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Datastore SOAP client for lifelong learning enrolment plugin
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos;

defined('MOODLE_INTERNAL') || die;

/**
 * Datastore SOAP client for lifelong learning enrolment plugin
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class soapclient extends \local_wiscservices\local\soap\wss_soapclient {

    /**
     * Returns the singleton instance of this class
     *
     * @exception SoapFault|Exception on error
     * @return soapclient|null
     */
    public static function get() {
        static $instance = null; // global instance of soapclient

        if ($instance === null) {
            $config = get_config("enrol_lifelonglearning");
            if (empty($config->soapuser)
                   || empty($config->soappass)
                   || empty($config->soapurl)) {
                throw new \moodle_exception('errornotconfigured', 'enrol_lifelonglearning');
            }
            $params = array( 'exceptions' => true,
                             'trace' => true,
                             'connection_timeout' => 20,
                             'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                           );

            $instance = new soapclient($config->soapurl, $params);
            $instance->__setUsernameToken($config->soapuser, $config->soappass);
            $instance->__setTimeout(20);
        }
        return $instance;
    }
}