<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Datastore interface for lifelong learning enrolment plugin
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos;

defined('MOODLE_INTERNAL') || die;

/**
 * Datastore interface for lifelong learning enrolment plugin
 *
 * @package    enrol_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
interface datasource {

    /**
     * Get rosters in lms target for given spaceids.
     *
     * @param string $lmstarget
     * @param string[] $spaceids
     * @return roster[]
     */
    public function getRosters($lmstarget, array $spaceids);

    /**
     * Get all spaces in lms target.
     *
     * @param string $lmstarget
     * @return space[]
     */
    public function getSpaces($lmstarget);
}