<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WISC Lifelong learning category.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos\schema;

defined('MOODLE_INTERNAL') || die;

/**
 * WISC Lifelong learning category.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class category extends base {

    const NS = 'http://services.wisc.edu/l3-lms/space';

    /** @var string id*/
    public $id;

    /** @var string description*/
    public $description;

    public function validate() {
        if (empty($this->id)) {
            throw new schema_exception('No category id');
        }
        if (empty($this->description)) {
            throw new schema_exception('No category description');
        }
    }
}