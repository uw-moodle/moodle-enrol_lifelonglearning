<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WISC Lifelong learning enrollment chagne event.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos\schema;

defined('MOODLE_INTERNAL') || die;

/**
 * WISC Lifelong learning enrollment change event.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class enrollment_event  extends base {

    const NS = 'http://services.wisc.edu/l3-lms/enrollment';

    /** @var string spaceId */
    public $spaceId;

    /** @var string role enum @see role_enum */
    public $role;

    /** @var bool active*/
    public $active;

    /** @var user user */
    public $user;

    public function init(array $data) {
        $data['user'] = new user($data['user']);
        // Active may be a boolean (from the SOAP interface) or the string "true"/"false" if we're parsing the XML directly.
        if (is_string($data['active'])) {
            $data['active'] = (0 == strcasecmp($data['active'], 'true'));
        }
        parent::init($data);
    }

    public function validate() {
        if (!role_enum::isvalid($this->role)) {
            throw new schema_exception('Invalid role');
        }
        if (is_null($this->spaceId)) {
            throw new schema_exception('No spaceId');
        }
        if (is_null($this->active)) {
            throw new schema_exception('No active status');
        }
        if (!$this->user instanceof user) {
            throw new schema_exception('Invalid user');
        }
        $this->user->validate();
    }

    public static function convert_from_xml(\SimpleXMLElement $xml) {
        return array_merge( (array) $xml->children(static::NS), (array) $xml->children(user::NS));
    }
}