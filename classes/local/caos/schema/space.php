<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WISC Lifelong learning space.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos\schema;

defined('MOODLE_INTERNAL') || die;

/**
 * WISC Lifelong learning space.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class space extends base {

    const NS = 'http://services.wisc.edu/l3-lms/space';

    /** @var string space id*/
    public $id;

    /** @var string lms target*/
    public $lmsTarget;

    /** @var category category. Can be null*/
    public $category;

    /** @var subcategory subcategory. Can be null*/
    public $subcategory;

    /** @var spaceProvisioning provisioning data*/
    public $spaceProvisioning;

    public function init(array $data) {
        // Clean up empty [sub]category elements from xml.
        if (isset($data['category'])) {
            $data['category'] = new category($data['category']);
            if (empty($data['category']->id)) {
                unset($data['category']);
                unset($data['subcategory']);
            }
        }
        if (isset($data['subcategory'])) {
            $data['subcategory'] = new subcategory($data['subcategory']);
            if (empty($data['subcategory']->id)) {
                unset($data['subcategory']);
            }
        }
        
        $data['spaceProvisioning'] = new spaceProvisioning($data['spaceProvisioning']);
        parent::init($data);
    }

    public function validate() {
        if (empty($this->id)) {
            throw new schema_exception('No space id');
        }
        if (empty($this->lmsTarget)) {
            throw new schema_exception('No lmsTarget');
        }
        if (!$this->spaceProvisioning instanceof spaceProvisioning) {
            throw new schema_exception('Invalid spaceProvisioning');
        }
        // Categories are optional
        if (!is_null($this->category) && !$this->category instanceof category) {
            throw new schema_exception('Invalid category');
        }
        if (!is_null($this->subcategory) && !$this->subcategory instanceof subcategory) {
            throw new schema_exception('Invalid subcategory');
        }
   }
}