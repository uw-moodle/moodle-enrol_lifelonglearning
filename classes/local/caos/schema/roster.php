<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WISC Lifelong learning roster
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos\schema;

defined('MOODLE_INTERNAL') || die;

/**
 * WISC Lifelong learning roster.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class roster extends base {

    const NS = 'http://services.wisc.edu/l3-lms/roster';

    /** @var string space id */
    public $spaceId;

    /** @var enrollment[] */
    public $enrollments = array();

    public function init(array $data) {
        $enrolobjects = array();
        if (!empty($data['enrollments'])) {
            foreach ($data['enrollments'] as $enroldata) {
                $enrolobjects[] = new enrollment($enroldata);
            }
        }
        $data['enrollments'] = $enrolobjects;
        parent::init($data);
    }

    public function validate() {
        if (empty($this->spaceId)) {
            throw new schema_exception('No space id');
        }
        foreach ($this->enrollments as $enrollment) {
            if (!$enrollment instanceof enrollment) {
                throw new schema_exception('Invalid enrollment');
            }
        }
    }

    public static function convert_from_xml(\SimpleXMLElement $xml) {
        return array_merge( (array) $xml->children(static::NS), (array) $xml->children(enrollment::NS));
    }
}