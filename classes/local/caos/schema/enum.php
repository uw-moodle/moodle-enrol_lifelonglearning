<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Basic enum class
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos\schema;

defined('MOODLE_INTERNAL') || die;

/**
 * Basic enum class
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class enum {

    /** Private constructor; no instantiations. */
    private function __construct() {}

    /** @var array private cache of class constants */
    private static $constCacheArray;

    /**
     * Get class constants.
     *
     * From @link http://stackoverflow.com/a/254543
     * @return array
     */
    private static function getConstants() {
        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new \ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    /**
     * Is it a valid enum value.
     *
     * @param mixed $value
     * @param bool $strict
     * @return bool
     */
    public static function isvalid($value, $strict = true) {
        $constants = self::getConstants();
        return in_array($value, array_values($constants), $strict);
    }
}