<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WISC Lifelong learning schema base class.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos\schema;

defined('MOODLE_INTERNAL') || die;

/**
 * WISC Lifelong learning schema base class.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class base {

    /**
     * XML namespace
     * @var string
     */
    const NS = '';

    /**
     * Construct object via an array of scalar initialization data.
     *
     * @param array|SimpleXMLElement $data
     * @throws schema_exception
     */
    final public function __construct($data = null) {
        if ($data instanceof \SimpleXMLElement) {
            $data = static::convert_from_xml($data);
        }
        if ($data) {
            $this->init($data);
            $this->validate();
        }
    }

    /**
     * Initialize instance of class from array data.
     *
     * The default implementation loops over class variables.  Override if there
     * are nested subclasses.
     *
     * @param array $data
     */
    public function init(array $data) {
        $vars = get_class_vars(get_called_class());
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $vars)) {
                $this->{$key} = $value;
            }
        }
    }


    /**
     * Convert from a \SimpleXMLElement to an array of initialization data.
     *
     * This may need to be overridden if an element uses multiple namespaces.
     *
     * @param \SimpleXMLElement $xml
     * @return \enrol_lifelonglearning\local\caos\schema\base
     */
    public static function convert_from_xml(\SimpleXMLElement $xml) {
        return (array) $xml->children(static::NS);
    }

    /**
     * Object validation, called from constructor.
     *
     * @throws schema_exception
     */
    abstract public function validate();
}