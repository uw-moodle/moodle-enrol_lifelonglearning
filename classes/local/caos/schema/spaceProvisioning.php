<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WISC Lifelong learning space.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_lifelonglearning\local\caos\schema;

defined('MOODLE_INTERNAL') || die;

/**
 * WISC Lifelong learning space provisioning type.
 *
 * @package    local_lifelonglearning
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class spaceProvisioning extends base {

    const NS = 'http://services.wisc.edu/l3-lms/space';

    /** @var string provisioned title */
    public $title;

    /** @var string provisioned short title */
    public $shortTitle;

    /** @var string provisioned start date ISO 8601 (YYYY-MM-DD).  Can be null. */
    public $startDate;

    /** @var string provisioned end date ISO 8601 (YYYY-MM-DD) Can be null. */
    public $endDate;

    public function validate() {
        if (empty($this->title)) {
            throw new schema_exception('No title');
        }
        // shortTitle, startDate and endDate are optional
    }
}